/*
 * geneKamel.h
 *
 *  Created on: 18.4.2014
 *      Author: antikamel
 */

#ifndef GENEKAMEL_H_
#define GENEKAMEL_H_

#include <vector>
#include <map>


using namespace std;
namespace kamelAI
{
	typedef enum
	{
		Input_current_piece_length = 0,
		Input_current_piece_angle = 1,
		Input_current_piece_radius = 2,
		Input_next_piece_length = 3,
		Input_next_piece_angle = 4,
		Input_next_piece_radius = 5,
		Input_angle  = 6,
		Input_current_pos = 7,
		Input_current_throttle = 8,
		Input_count = 9
	}EInput;

	class geneKamel
	{

	public:
		geneKamel();
		geneKamel(string filename);
		~geneKamel();
		void randomize();
		void updateInput( EInput input, double value );
		double getOutput();

		void saveResult(string filename);
		void load(string filename);
		void saveResult(double result);/// Tell that we crashed = bad score


		/// Apply genetics. If file has enough samples
		void genetics(string filename);

	private:
		double mInputs[Input_count];
		double mModifiers[ Input_count*Input_count ];


	};

}

#endif /* GENEKAMEL_H_ */
