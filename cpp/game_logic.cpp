#include "game_logic.h"
#include "protocol.h"
#include <sys/time.h>

using namespace hwo_protocol;

game_logic::game_logic()
  : action_map
    {
      { "join", &game_logic::on_join },
      { "gameStart", &game_logic::on_game_start },
      { "carPositions", &game_logic::on_car_positions },
      { "crash", &game_logic::on_crash },
      { "gameEnd", &game_logic::on_game_end },
      { "gameInit", &game_logic::on_init },
      { "error", &game_logic::on_error },
      { "yourCar", &game_logic::on_your_car },
      { "finish", &game_logic::on_finish },
      { "tournamentEnd", &game_logic::on_tournament_end },
      { "spawn", &game_logic::on_spawn },
      { "dnf", &game_logic::on_dnf },
      { "lapFinished", &game_logic::lap }
    }
{
	mLastPiece =-1;
	mAI.randomize();
	mCurrThrottle = 0;
	mInTrack = true;
}

game_logic::msg_vector game_logic::react(const jsoncons::json& msg)
{
  const auto& msg_type = msg["msgType"].as<std::string>();
  const auto& data = msg["data"];
  try
  {
	  mCurrTick = msg["gameTick"].as_int();
  }
  catch (const std::exception& e)
  {
	  std::cerr << "no tick" << std::endl;
  }

  auto action_it = action_map.find(msg_type);
  if (action_it != action_map.end())
  {
    return (action_it->second)(this, data);
  }
  else
  {
    std::cout << "Unknown message type: " << msg_type << std::endl;
    return { make_ping() };
  }
}

game_logic::msg_vector game_logic::on_join(const jsoncons::json& data)
{
  std::cout << "Joined" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_start(const jsoncons::json& data)
{
  std::cout << "Race started" << std::endl;
  mCurrTick = 0;

  return { make_ping() };
}

game_logic::msg_vector game_logic::on_car_positions(const jsoncons::json& data)
{
	//std::cout << "Race  " << pretty_print(data) << "#" << data.size() <<  std::endl;
	if(mInTrack)
	for ( int i = 0 ; i < data.size() ; i++)
	{
		try
		{

			if ( data[i]["id"]["name"].as<std::string>().compare("antikamel") == 0)
			{
				//std::cout << mCurrTick << "My id is "<< i << std::endl;

				int piece = data[i]["piecePosition"]["pieceIndex"].as_int();
				double pos = data[i]["piecePosition"]["inPieceDistance"].as_double();
				double angle = data[i]["angle"].as_double();

				if ( piece < mLastPiece)
				{
					mLap++;

					std::cout << "Lap result " << mCurrTick << std::endl;
					if (mLap == 2)
					{
						mLap == 0;
						mAI.saveResult(mCurrTick);
						mCurrTick = 0;
						mAI.randomize();
					}
				}

				int nextPiece = (piece+1)%mTrack.size();

				mAI.updateInput(kamelAI::Input_current_piece_length, mTrack[piece].length);
				mAI.updateInput(kamelAI::Input_current_piece_angle,mTrack[piece].angle);
				mAI.updateInput(kamelAI::Input_current_piece_radius,mTrack[piece].radius);
				mAI.updateInput(kamelAI::Input_next_piece_length,mTrack[nextPiece].length);
				mAI.updateInput(kamelAI::Input_next_piece_angle,mTrack[nextPiece].angle);
				mAI.updateInput(kamelAI::Input_next_piece_radius,mTrack[nextPiece].radius);
				mAI.updateInput(kamelAI::Input_angle,angle);
				mAI.updateInput(kamelAI::Input_current_pos,pos);
				mAI.updateInput(kamelAI::Input_current_throttle,mCurrThrottle);

				mCurrThrottle = mAI.getOutput();
				if ( mCurrThrottle < 0.1) // bad
					mAI.randomize();

				if (piece != mLastPiece)
				{
					std::cout << "Angle " << angle << " Pos " << piece << " : " << pos << std::endl;
					std::cout << "Speed" << mCurrThrottle << std::endl;
				}

				mLastPiece = piece;


			}

		}
		catch (const std::exception& e)
		{
			std::cerr << e.what() << std::endl;
		}
	}
   return { make_throttle(mCurrThrottle>0.01?mCurrThrottle:0.01) };
}

game_logic::msg_vector game_logic::on_crash(const jsoncons::json& data)
{
	try
	{

		if ( data["name"].as<std::string>().compare("antikamel") == 0)
		{
			std::cout << "I crashed :("<< std::endl;
		//	mAI.saveResult(-666);
			mInTrack=false;
		}

	}
	catch (const std::exception& e)
	{
		std::cerr << e.what() << std::endl;
	}
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_end(const jsoncons::json& data)
{
  std::cout << "Race ended " << data.to_string() <<std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_error(const jsoncons::json& data)
{
  std::cout << "Error: " << data.to_string() << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_init(const jsoncons::json& data)
{
 // std::cout << "Init: " << pretty_print(data) << std::endl;
  for (int i = 0 ; i < data["race"]["track"]["pieces"].size() ; i++)
  {

	  mTrack.push_back(TPiece());
	  mTrack[mTrack.size()-1].angle = data["race"]["track"]["pieces"][i].has_member("angle")?data["race"]["track"]["pieces"][i]["angle"].as_double():0;
	  mTrack[mTrack.size()-1].length = data["race"]["track"]["pieces"][i].has_member("length")?data["race"]["track"]["pieces"][i]["length"].as_double():0;
	  mTrack[mTrack.size()-1].radius = data["race"]["track"]["pieces"][i].has_member("radius")?data["race"]["track"]["pieces"][i]["radius"].as_double():0;
	  std::cout << "#"<< i<< " l" <<mTrack[mTrack.size()-1].length << " a" <<mTrack[mTrack.size()-1].angle << " r" << mTrack[mTrack.size()-1].radius <<   std::endl;
  }

  return { make_ping() };
}

game_logic::msg_vector game_logic::on_your_car(const jsoncons::json& data)
{
	  std::cout << "YourCar: " << data.to_string() << std::endl;

	  return { make_ping() };
}

game_logic::msg_vector game_logic::on_tournament_end(const jsoncons::json& data)
{
	  std::cout << "Tournament End: " << data.to_string() << std::endl;

	  return { make_ping() };
}

game_logic::msg_vector game_logic::on_spawn(const jsoncons::json& data)
{
	try
	{

		if ( data["name"].as<std::string>().compare("antikamel") == 0)
		{
			std::cout << "I spawned :)"<< std::endl;
		//	mAI.saveResult(-666);
			mInTrack=true;
		}

	}
	catch (const std::exception& e)
	{
		std::cerr << e.what() << std::endl;
	}
	  return { make_ping() };
}

game_logic::msg_vector game_logic::on_dnf(const jsoncons::json& data)
{
	  std::cout << "DNF: " << data.to_string() << std::endl;

	  return { make_ping() };
}

game_logic::msg_vector game_logic::on_finish(const jsoncons::json& data)
{
	  std::cout << "Finish: " << data.to_string() << std::endl;

	  return { make_ping() };
}

game_logic::msg_vector game_logic::lap(const jsoncons::json& data)
{
	std::cout << "Lap result " << std::endl;
	try
	{

		if ( data["car"]["name"].as<std::string>().compare("antikamel") == 0)
		{
			std::cout << "Lap result " << data["lap"]["time"]["millis"].as_double() << std::endl;
			mAI.saveResult(data["lap"]["time"]["millis"].as_double());
			mAI.randomize();
		}

	}
	catch (const std::exception& e)
	{
		std::cerr << e.what() << std::endl;
	}
	return { make_ping() };
}
