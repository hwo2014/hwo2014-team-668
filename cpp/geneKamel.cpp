/*
 * geneKamel.cpp
 *
 *  Created on: 18.4.2014
 *      Author: antikamel
 */

#include "geneKamel.h"
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <iostream>
#include <iostream>
#include <fstream>
#include <iomanip>
#define random(HI,LO) LO + static_cast <double> (rand()) /( static_cast <double> (RAND_MAX/(HI-LO)));

using namespace std;
using namespace kamelAI;

double sigmoid(double x)
{
     return 1 / (1 + exp((double) -x));
}



geneKamel::geneKamel()
{
	for (int i = 0; i <  Input_count*Input_count ; i++)
	{
		mModifiers[i] = 0.0;
	}
}

void geneKamel::randomize()
{
	srand (time(NULL));
	std::cout << "Randomize" << std::endl;
	for (int i = 0 ; i < Input_count ; i++ )
	{
		for (int j = i ; j < Input_count ; j++ )
		{
			mModifiers[i*Input_count+j] = random(-100,100);
			std::cout << i << " -> " << j << " == " << mModifiers[i*Input_count+j] << std::endl;
		}
	}

}

geneKamel::geneKamel(string filename)
{

}
geneKamel::~geneKamel()
{

}
void geneKamel::updateInput( EInput input, double value )
{
	mInputs[(int)input] = value*value;
}

double geneKamel::getOutput()
{
	//std::cout << "Calculate output " << std::endl;
	double sum = 0;

	for (int i = 0 ; i < Input_count ; i++ )
	{
		for (int j = i ; j < Input_count ; j++ )
		{
			sum += sigmoid(mInputs[i]*mInputs[j]*mModifiers[i*Input_count+j]);
		}
	}
	//std::cout << "Sum " << sum << " sig "<<  sigmoid(sum) << std::endl;
	return sigmoid(sum);
}

void geneKamel::saveResult(double score)
{
	std::cout << "Write result " << score << std::endl;
	ofstream myfile ("gene.txt");
	//std::string line;
	myfile <<  std::fixed << std::setprecision(20);
	myfile << score << ",";

	for (int i = 0 ; i < Input_count ; i++ )
	{
		for (int j = i ; j < Input_count ; j++ )
		{
			myfile << mModifiers[i*Input_count+j]<< ",";
		}
	}
	myfile << std::endl;
	myfile.close();


}

void geneKamel::load(string filename)
{

}

/// Apply genetics. If file has enough samples
void geneKamel::genetics(string filename)
{

}
