#include <errno.h>
#include <netdb.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include <stdlib.h>
#include "cJSON.h"
#include <math.h>
#include <pthread.h>

static cJSON *ping_msg();
static cJSON *join_msg(char *bot_name, char *bot_key);
//static cJSON *joinduel_msg(char *bot_name, char *bot_key);
static cJSON *join_germany_msg(char *bot_name, char *bot_key, int cars);
static cJSON *join_usa_msg(char *bot_name, char *bot_key, int cars);
static cJSON *join_suomi_msg(char *bot_name, char *bot_key, int cars);
static cJSON *join_france_msg(char *bot_name, char *bot_key, int cars);
static cJSON *throttle_msg(double throttle);
static cJSON *right_lane_msg();
static cJSON *left_lane_msg();
static cJSON *turbo_msg();
static cJSON *make_msg(char *type, cJSON *msg);
static cJSON *read_msg(int fd);
static void write_msg(int fd, cJSON *msg);
static cJSON *createRace_msg(char *bot_name, char *bot_key);
static cJSON *joinRace_msg(char *bot_name, char *bot_key);
int countlines(const char* filename);
double getOutput();
void setInput(int input, double value);
/*
#define		Input_current_piece_length 0
#define		Input_current_piece_angle2 1
#define		Input_current_piece_angle 2
#define		Input_current_piece_radius 3
//#define		Input_current_piece_switch 4
#define		Input_next_piece_length 4
#define		Input_next_piece_angle2 5
#define		Input_next_piece_angle 6
#define		Input_next_piece_radius 7
//#define		Input_next_piece_switch 9
#define		Input_angle  8
#define		Input_angle2 9
#define		Input_current_pos 10
//#define		Input_current_throttle 11
//#define		Input_current_lane 14
//#define		Input_max_lane 15
#define		Input_current_lane_dist 11
//#define		Input_car_width 17
//#define		Input_car_length 18
//#define		Input_car_flag 19
#define		Input_car_velocity 12

#define		Input_next_turn_dist 13
#define		Input_next_turn_a 14
#define		Input_next_turn_r 15
*/

#define S(r) atan( 1.0 / (sqrt(2.0*r-1.0)) )*180.0/ 3.14159265

#define 	Input_Velocity 					0
#define		Input_Angle						1
#define		Input_Angle2					2
#define		Input_DistanceToTurn			3
#define		Input_LengthOfNextTurn			4
#define		Input_TurnAngle					5
#define		Input_TurnAngle2				6
#define		Input_TurnRad					7
#define		Input_Count 					8

#define		Variable_TakeInnerLane			0
#define		Variable_AntiSlipAccMinVelocity 1
#define		Variable_AntiSlipAccLength		2
#define		Variable_AntiSlipMinAngle		3
#define		Variable_SlowDownLength			4
#define		Variable_StraightLaneAntiSlip   5
#define		Variable_UseKnownFast			6
#define		Variable_MinTurboLenghtPerTick  7
#define		Variable_Tactics				8
#define		Variable_EnterCornerSpeedLimit  9
#define		Variable_EnterCornerSpeedDist   10
#define		Variable_Count					11


#define 		PIECE_LENGTH 0
#define 		PIECE_ANGLE  1
#define 		PIECE_RADIUS 2
#define 		PIECE_SWITCH 3
#define 		PIECE_CARS   4
#define 		PIECE_COUNT  5

#define			RANGE 3

int 			g_generation;

double** 		g_track;
double* 		g_tracklencum;
double* 		g_track_knownfast;

int				g_trackLen;
char			g_myname[32];
int 			g_trackInitialized;
int				g_inTrack;
int             g_hasScore;
int				g_switching;

int				g_lanesLen;
double*			g_lanes;

double			g_carLength;
double			g_carWidth;

double			g_carFlagpos;

int				g_weightsLen;
double*			g_weights;
double*			g_variables;
//int				g_variablesLen;
double*			g_inputs;
double			g_output;

double			g_throttle;

int				g_current_tick;
int				g_last_current_tick;
int				g_startTick;
int				g_endTick;
double			g_goFaster;

int				g_abort;

double			g_pos;
int				g_piece;
int				g_laps;
double			g_score;

int				g_crash;
int				g_ready;
int				g_mode;
double 			g_vel;
double			g_maxSpeed;
double			g_bestLap;
#define carInfo_pieceN 0
#define carInfo_pieceD 1
#define carInfo_crashed 2
#define carInfo_totalDistance 3
#define carInfo_velocity 4
#define carInfo_lane 5
#define carInfo_count 6
double**			g_allCarsInfo;
int					g_allCarsInfoN;




#define type_random 1
#define type_mutate 2
#define type_crossover 3
#define type_static 4
int				g_type;

int 			g_totalLaps;

int				g_lane;
int				g_changing_lane;

int			g_generationSize;
int			g_generationBest;

double		g_trackTotalLength;
double		g_myTrackPos;

int 		g_turboavailable;
int			g_turbotick;

int			g_slowdown;
int			g_antiskip;

int 		g_verbose;

#define DEBUG if(g_verbose==1)



void clear_trackInfo()
{
	DEBUG puts("Cleanup track");
	int i;
	if ( g_track )
	{

		for ( i = 0; i < g_trackLen ; i++ )
		{
			if (g_track[i])
			{
				free (g_track[i]);
			}
		}
		free(g_track);
	}


	if (g_lanes)
	{
		free (g_lanes);
	}

	if(g_tracklencum)
	{
		free (g_tracklencum);
	}

	if (g_track_knownfast)
	{
		free(g_track_knownfast);
	}
	DEBUG puts("Cleanup done");
}


void initCarInfo(int n)
{
	return;
	/*
	if ( g_allCarsInfo)
	{
		puts("error alloc car info already allocated");
		exit(2);
	}
	else
	{
		g_allCarsInfo = (double**)malloc(sizeof(double*)*n);
		if ( !g_allCarsInfo)
		{
			puts("error alloc car info");
			exit(2);
		}
		int i;
		for (i = 0; i < g_allCarsInfoN ; i++)
		{
			g_allCarsInfo[i] = (double*)malloc(sizeof(double)*carInfo_count);
			if ( g_allCarsInfo[i] == 0)
			{
				puts("error sub alloc car info");
				exit(2);
			}
			int k;
			for (k = 0; k < carInfo_count ; k++)
			{
				g_allCarsInfo[i][k] = 0;
			}

		}
	}
	*/
}

void clearCarInfo()
{
	/*
	DEBUG puts("Clear car info");
	if (  g_allCarsInfo )
	{
		int i;
		for (i = 0; i < g_allCarsInfoN ; i++)
		{
			if ( g_allCarsInfo[i]) free (g_allCarsInfo[i]);
		}
		free(g_allCarsInfo);
	}
	g_allCarsInfo = 0;
	DEBUG puts("Clear car info done");
	*/
}

void clear_allocations()
{
	DEBUG puts("Cleanup memory");

	if (g_weights)
	{
		free (g_weights);
	}

	if ( g_inputs)
	{
		free (g_inputs);
	}

	if ( g_variables)
	{
		free (g_variables);
	}
	DEBUG puts("Cleanup memory done");

	clear_trackInfo();
	clearCarInfo();
}
#define ERRORFAIL { printf("ERROR %d\n", __LINE__);exit(1);}


/// Util
double getRandomdouble(double min, double max)
{
	double f = (double)rand() / RAND_MAX;
	return min + f * (max - min);
}

/// GA

void initializeGA()
{

	g_inputs = (double*)malloc( sizeof(double)*Input_Count );
	if (!g_inputs) ERRORFAIL;

	g_weights = (double*)malloc( sizeof(double)* g_weightsLen);
	if (!g_weights) ERRORFAIL;
	int i;
	for (i = 0; i <  g_weightsLen ; i++)
	{
		g_weights[i] = 0.0;
	}

	g_variables = (double*)malloc(sizeof(double)*Variable_Count);

}


/// Connection stuff
static void error(char *fmt, ...)
{
    char buf[BUFSIZ];

    va_list ap;
    va_start(ap, fmt);
    vsnprintf(buf, BUFSIZ, fmt, ap);
    va_end(ap);

    if (errno)
        perror(buf);
    else
        fprintf(stderr, "%s\n", buf);

    exit(1);
}

static int connect_to(char *hostname, char *port)
{
    int status;
    int fd;
    char portstr[32];
    struct addrinfo hint;
    struct addrinfo *info;

    memset(&hint, 0, sizeof(struct addrinfo));
    hint.ai_family = PF_INET;
    hint.ai_socktype = SOCK_STREAM;

    sprintf(portstr, "%d", atoi(port));

    status = getaddrinfo(hostname, portstr, &hint, &info);
    if (status != 0) error("failed to get address: %s", gai_strerror(status));

    fd = socket(PF_INET, SOCK_STREAM, 0);
    if (fd < 0) error("failed to create socket");

    status = connect(fd, info->ai_addr, info->ai_addrlen);
    if (status < 0) error("failed to connect to server");

    freeaddrinfo(info);
    return fd;
}

static void log_message(char *msg_type_name, cJSON *msg)
{
    cJSON *msg_data;
    //printf("data %s\n",msg_type_name);
    if (!strcmp("join", msg_type_name)) {
        puts("Joined");
    } else if (!strcmp("lapFinished", msg_type_name)) {
        puts("lapFinished");
        cJSON* data = cJSON_GetObjectItem(msg, "data");
        {
        	cJSON* car = cJSON_GetObjectItem(data,"car");
        	if ( car)
        	{
        		cJSON* name = cJSON_GetObjectItem(data,"name");

        		if (name && strcmp(name->valuestring, g_myname)==0)
        		{

        			DEBUG puts("My lap");
        			cJSON* lap = cJSON_GetObjectItem(data,"lapTime");
        			if (lap)
        			{

        				double t = lap->valuedouble;
        				DEBUG printf("LAP TIME %lf\n", t);
        				if (t > g_bestLap)
        				{
        					g_bestLap = t;
        					DEBUG printf("Best lap!\n");
        				}
        			}

        		}
        	}
        }
    }else if (!strcmp("gameStart", msg_type_name)) {
        puts("Race started");
    } else if (!strcmp("crash", msg_type_name)) {


    	DEBUG puts("crash");
        cJSON* data = cJSON_GetObjectItem(msg, "data");
        if (data)
        {
        	cJSON* name = cJSON_GetObjectItem(data,"name");
        	if ( name)
        	{
        		if ( strcmp(name->valuestring, g_myname)==0)
        		{
        			g_crash++;
        			DEBUG puts("I crashed :(");
        			g_inTrack= 0;
        		}
        	}
        }

    } else if (!strcmp("gameEnd", msg_type_name)) {

        g_trackInitialized = 0;
        g_inTrack = 0;
        DEBUG puts("Race ended");
        cJSON* data = cJSON_GetObjectItem(msg, "data");
        if (data)
        {
        	cJSON* bestlap = cJSON_GetObjectItem(data, "bestLaps");
        	if (bestlap)
        	{
        		int carsN =  cJSON_GetArraySize(bestlap);
        		int i;
        		for (i = 0 ; i < carsN ; i++)
        		{
        			cJSON * car = cJSON_GetArrayItem(bestlap, i);


        			cJSON * carid = cJSON_GetObjectItem(car, "car");
					if (carid)
					{

						cJSON * carname = cJSON_GetObjectItem(carid, "name");

						if (carname)
						{
							DEBUG printf("carName : %s\n", carname->valuestring);
							if ( strcmp(carname->valuestring, g_myname)==0)
							{
								DEBUG printf("That's me\n");
								cJSON * result = cJSON_GetObjectItem(car, "result");
								if (result)
								{
									cJSON * millis = cJSON_GetObjectItem(result, "millis");
									if (millis)
									{
										double m = millis->valuedouble;
										DEBUG printf("Millis %lf\n", m);
										g_score = m;
										if (g_score < 0)
										{
											DEBUG puts("Negative time!");
										}
										g_hasScore = 1;

									}
								}
							}
						}

					}
        		}
        	}
        }
        clear_trackInfo();
        /*
         *
        "bestLaps": [
            {
              "car": {
                "name": "Schumacher",
                "color": "red"
              },
              "result": {
                "lap": 2,
                "ticks": 3333,
                "millis": 20000
              }
            },
            {
              "car": {
                "name": "Rosberg",
                "color": "blue"
              },
              "result": {}
            }
          ]
          */


    } else if (!strcmp("gameInit", msg_type_name)) {
    	DEBUG puts("Race init");

    	        cJSON* data = cJSON_GetObjectItem(msg, "data");
    	        if ( data )
    	        {
    	        	DEBUG puts("Data");
    	        	cJSON* race = cJSON_GetObjectItem(data, "race");
    	        	if (race)
    	        	{
    	        		DEBUG puts("Race");
						cJSON* track  = cJSON_GetObjectItem(race, "track");
						if (track )
						{
							DEBUG puts("track");
							cJSON * pieces = cJSON_GetObjectItem(track,"pieces");
							if (pieces)
							{
								DEBUG puts("pieces");
								int i;
								g_trackLen =  cJSON_GetArraySize(pieces);
								DEBUG printf("%d track pieces\n", g_trackLen);
								g_track = (double**)malloc(sizeof(double*) * g_trackLen);
								if(!g_track)
								{
									ERRORFAIL;
								}

								g_tracklencum = (double*)malloc(sizeof(double) * g_trackLen );
								if(!g_tracklencum)
								{
									ERRORFAIL;
								}

								g_track_knownfast = (double*)malloc(sizeof(double) * g_trackLen );
								if(!g_track_knownfast)
								{
									ERRORFAIL;
								}
								double l = 0;
								for (i = 0 ; i < g_trackLen ; i++)
								{
									g_track[i] = (double*) malloc(sizeof(double)*PIECE_COUNT);

									if (g_track[i]==NULL)ERRORFAIL;

									cJSON * piece = cJSON_GetArrayItem(pieces, i);
									if (piece)
									{
										cJSON * length = cJSON_GetObjectItem(piece, "length");
										cJSON * angle = cJSON_GetObjectItem(piece, "angle");
										cJSON * radius = cJSON_GetObjectItem(piece, "radius");
										cJSON * sw = cJSON_GetObjectItem(piece, "switch");

										g_track[i][PIECE_CARS]=0;
										if ( length != NULL)
										{
											g_track[i][PIECE_LENGTH] = length->valuedouble;
										}
										else
										{
											g_track[i][PIECE_LENGTH] = 0.0;
										}

										if ( angle != NULL)
										{
											g_track[i][PIECE_ANGLE] = angle->valuedouble;
										}
										else
										{
											g_track[i][PIECE_ANGLE] = 0.0;
										}

										if ( radius != NULL)
										{
											g_track[i][PIECE_RADIUS] = radius->valuedouble;
										}
										else
										{
											g_track[i][PIECE_RADIUS] = 0.0;
										}

										if ( sw != NULL && sw->type == cJSON_True)
										{
											g_track[i][PIECE_SWITCH] = 1.0;
										}
										else
										{
											g_track[i][PIECE_SWITCH] = 0.0;

										}
#define PI 3.14159265359
										if ( fabs(g_track[i][PIECE_ANGLE]) > 0 )
										{
											g_track[i][PIECE_LENGTH] = ( fabs(g_track[i][PIECE_ANGLE])*PI*g_track[i][PIECE_RADIUS])/180.0 ;
										}

										g_tracklencum[i] = l;
										l += g_track[i][PIECE_LENGTH];
										g_track_knownfast[i] = -100;
										DEBUG printf("#%d\tLen%f A%f R%f S%d\tSUM %lf\n",
												i, g_track[i][PIECE_LENGTH],g_track[i][PIECE_ANGLE],
												g_track[i][PIECE_RADIUS],(int)g_track[i][PIECE_SWITCH], g_tracklencum[i]);



									}
								}
							}
						} // Pieces
						cJSON * lanes = cJSON_GetObjectItem(track,"lanes");
						if (lanes)
						{
							DEBUG puts("Lanes");
							g_lanesLen =  cJSON_GetArraySize(lanes);
							g_lanes = (double*)malloc(g_lanesLen*sizeof(double));
							if (!g_lanes)ERRORFAIL;
							int i;
							for (i = 0 ; i < g_lanesLen ; i++)
							{
								cJSON * lane = cJSON_GetArrayItem(lanes, i);
								cJSON * dist = cJSON_GetObjectItem(lane, "distanceFromCenter");
								if (dist)
								{
									g_lanes[i] = dist->valuedouble;
								}
								else
								{
									g_lanes[i] = 0;
									puts("No lane distanceFromCenter");
								}
								DEBUG printf("#%d\tDist%f\n",i,g_lanes[i]);
							}




						} /// Lanes


    	        	} /// race
					cJSON * cars = cJSON_GetObjectItem(race,"cars");
					if (cars)
					{
						clearCarInfo();
						initCarInfo(cJSON_GetArraySize(cars));
						printf("Cars #%d\n",cJSON_GetArraySize(cars));
						int i;
						for (i = 0 ; i < cJSON_GetArraySize(cars) ; i++)
						{
							cJSON * car = cJSON_GetArrayItem(cars, i);
							if (car)
							{
								cJSON * carid = cJSON_GetObjectItem(car, "id");
								if (carid)
								{

									cJSON * carname = cJSON_GetObjectItem(carid, "name");
									cJSON * carcolor = cJSON_GetObjectItem(carid, "color");
									if (carname && carcolor)
									{
										printf("Car #%d\t%s (%s) \n", i,carname->valuestring,carcolor->valuestring);
										if (carname && strcmp(carname->valuestring,g_myname)==0)
										{
											DEBUG puts("I'm racing :)");
										}

										cJSON * cardim = cJSON_GetObjectItem(car, "dimensions");
										if (cardim)
										{
											cJSON * carlen = cJSON_GetObjectItem(cardim, "length");
											cJSON * carwidth = cJSON_GetObjectItem(cardim, "width");
											cJSON * carflag = cJSON_GetObjectItem(cardim, "guideFlagPosition");

											if (carlen)
											{
												g_carLength = carlen->valuedouble;
											}
											else
											{
												DEBUG puts("No car length");
											}

											if (carwidth)
											{
												g_carWidth = carwidth->valuedouble;
											}
											else
											{
												DEBUG puts("No car width");
											}

											if (carflag)
											{
												g_carFlagpos = carflag->valuedouble;
											}
											else
											{
												DEBUG puts("No car flag position");
											}

											DEBUG printf("Car L%f x W%f  (%f)\n",g_carLength,g_carWidth,g_carFlagpos);

										}
									}


								}



							}
							else
							{
								puts("Cannot read car");
							}
						}
						/// Check that I am on the race

					} /// cars
					else
					{
						puts("No cars");
					}

					cJSON * info = cJSON_GetObjectItem(race,"raceSession");
					if (info)
					{
						DEBUG printf("raceSession\n");
						cJSON * laps = cJSON_GetObjectItem(info,"laps");
						cJSON * quickRace = cJSON_GetObjectItem(info,"quickRace");
						if (laps)
						{
							g_totalLaps = laps->valueint;

						}
						else
						{
							DEBUG puts("No number of laps?");
							DEBUG puts("Assume 5. 5 is nice number of traks. This may be a qualification round...");
							g_totalLaps=5;

							///ERRORFAIL;

						}

						if (quickRace)
						{
							if (quickRace->type == cJSON_True)
							{
								printf("Quickrace\n");
							}
							else if (quickRace->type == cJSON_True)
							{
								printf("Not quickrace\n");
								printf("This might be an actual competition :)\n");
							}
							else
							{
								printf("No quickrace info...\n");
							}
						}
						else
						{
							puts("No quickrace info...");
						}


						g_trackTotalLength = g_totalLaps * (g_tracklencum[ g_trackLen-1] + g_track[ g_trackLen-1][PIECE_LENGTH] );


						/// Check that I am on the race

					} /// cars
					else
					{
						DEBUG puts("No cars");
					}


    	        }/// data
    	        else
    	        {
    	        	DEBUG puts("No data!");
    	        }

    	        DEBUG puts("Race init done");
    	        g_trackInitialized = 1;
    	        g_inTrack = 1;


    }
    else if (!strcmp("spawn", msg_type_name))
    {
    	DEBUG puts("spawn");
    	cJSON* data = cJSON_GetObjectItem(msg, "data");
		if (data)
		{
			cJSON* name = cJSON_GetObjectItem(data,"name");
			if ( name)
			{
				if ( strcmp(name->valuestring, g_myname)==0)
				{

					DEBUG puts("I spawned :)");
					g_inTrack= 1;
				}
			}
		}

    } else if (!strcmp("finish", msg_type_name)) {
    	DEBUG puts("finish");
    } else if (!strcmp("dnf", msg_type_name)) {
    	DEBUG puts("Did not finish");
    } else if (!strcmp("turboAvailable", msg_type_name)) {
    	DEBUG puts("Turbo");
        g_turboavailable++;
        g_turbotick = 30;
        cJSON* tick = cJSON_GetObjectItem(msg, "turboDurationTicks");
        if (tick )
        {
        	g_turbotick = tick->valueint;
        }
        else
        {
        	puts("No turbotick!");
        }
    }else if (!strcmp("tournamentEnd", msg_type_name)) {
        DEBUG puts("/////// TOURNAMENT END /////////");
        g_ready = 1;

    }else if (!strcmp("error", msg_type_name)) {
        msg_data = cJSON_GetObjectItem(msg, "data");
        if (msg_data == NULL)
            puts("Unknown error");
        else
            printf("ERROR: %s\n", msg_data->valuestring);
    }
}
#define CLOCKTYPE CLOCK_MONOTONIC
int main(int argc, char *argv[])
{
	puts("Antikamel racer bot WRONG BUILD!!!\n");
	return 1;
    int sock;
    cJSON *json;
    g_mode = 0;
    int botNum = 0;
    int raceNum = 0;

    /***
     *  Prepare stuff
     */
    g_goFaster = 0;
    g_bestLap = 0;
    g_maxSpeed = 0;
    g_variables = 0;
    g_weights = 0;
    g_slowdown=-1;
    g_antiskip=-1;
    g_allCarsInfo = 0;
    g_type = 0;
    g_tracklencum = 0;
    g_trackInitialized = 0;
    g_trackLen = 0;
    g_track = NULL;
    g_inTrack = 0;
    g_lanes = NULL;
    g_throttle = 0;
    g_carLength = 40;
    g_carWidth = 20;
    g_carFlagpos = 0;
    g_abort = 0;
    g_crash = 0;
    g_pos = 0;
    g_weightsLen = 0;
    g_piece = 0;
    g_startTick = -1;
    g_laps = 0;
    g_changing_lane = 0;
    g_score=0.0;
    g_ready = 0;
    g_turboavailable = 0;
    g_totalLaps = 3;
    g_hasScore=0;
    g_switching = 0 ;
    g_track_knownfast = 0;
    g_trackTotalLength = 0;
    int maxCrash = 0;
    g_last_current_tick = 1;

    //// Initialize GA
    //getWriteLock();
    srand((time(NULL) & 0xFFFF) | (getpid() << 16));
    srandom((time(NULL) & 0xFFFF) | (getpid() << 16));
   // srand(time(NULL));
   // srandom(time(NULL));

    memset(g_myname,'\0',32);
    strcpy(g_myname,argv[3] );
    /*
    if ( g_mode == 1)
    {

		resolveWeightsLen();
		getGenerationSize();
		setGeneration();
		initializeGA();
		setWeights();

    }
    else if (g_mode==0)
    {
    	/// mode == 0

		getGenerationSize();
		setGeneration();
    	resolveWeightsLen();
    	initializeGA();
    	setupBot(0);

    }
    else
    {

    	    	/// mode ==
    	puts("Final testing");
		getGenerationSize();
		//setGeneration();
		resolveWeightsLen();
		initializeGA();
		botNum = 0;
		setupBot(botNum);

		char name[16];
	    sprintf(name , "%s_%d",argv[3] , botNum );
	    memset(g_myname,'\0',32);
	    strcpy(g_myname, name);



    }
    */
   // freeWriteLock();

    /**
     *
     */
    g_verbose = 1;

    sock = connect_to(argv[1], argv[2]);


	//g_verbose = 1;
	maxCrash = 1;
	int race = -1;

	int cars = (g_generation/8)-1;
	if (cars < 1 ) cars = 1;
	if (cars > 4) cars = 4;
	maxCrash+=cars;

	if (race == 0)
	{
		puts("Join germany");
		json = join_germany_msg(g_myname, argv[4],cars);

	}
	if (race == 1)
	{
		puts("Join france");
		json = join_france_msg(g_myname, argv[4],cars);

	}
	if (race == 2)
	{
		puts("Join usa");
		json = join_usa_msg(g_myname, argv[4],cars);
	}
	if (race == 3)
	{
		puts("Join keimola");
		json = join_suomi_msg(g_myname, argv[4],cars);
	}

	if (race == -1)
	{
		puts("Join default");
		json = join_msg(argv[3], argv[4]);
	}
	DEBUG puts("Write join");
	json = join_msg(argv[3], argv[4]);
	write_msg(sock, json);



    //cJSON_Delete(json);
    printf("Start %s\n", g_myname);
    cJSON_Delete(json);
    json = 0;
    while ((json = read_msg(sock)) != NULL)
    {
	  struct timespec tsi, tsf;

	  clock_gettime(CLOCKTYPE, &tsi);

	  DEBUG printf("\n");


		int sent = 0;
		cJSON *msg, *msg_type;
		msg = 0;

		char *msg_type_name;
		g_current_tick = -1;
		cJSON *tickmsg = cJSON_GetObjectItem(json, "gameTick");
		msg_type = cJSON_GetObjectItem(json, "msgType");
		if (msg_type == NULL)
			error("missing msgType field");
		if (tickmsg)
		{
			g_current_tick = tickmsg->valueint;
			//DEBUG printf("%s\n",msg_type->valuestring);
			//DEBUG printf("G%d Tick %d -> %d : %d\t\n",g_generation,g_startTick ,g_current_tick, g_current_tick-g_startTick);
			DEBUG printf("G%d",g_generation);
			if (g_verbose)
			{
				if (g_type == type_random) printf("R");
				else if (g_type == type_static) printf("S");
				else if (g_type == type_mutate) printf("M");
				else if (g_type == type_crossover) printf("C");
				else printf("?");
			}

			DEBUG printf(" %3.1lf LAP %d\n", g_myTrackPos/(g_trackTotalLength)*100.0,g_laps );
		}





        msg_type_name = msg_type->valuestring;
        if (!strcmp("carPositions", msg_type_name))
        {
        	int o;
			for (o = 0 ; o < g_trackLen ; o++)
				g_track[o][PIECE_CARS]=0;
        	int mydata = -1;
        	if (!g_trackInitialized)
        	{
        		printf("Game is running but track has not been initialized! :(\n");
        	}

        	if (!g_inTrack)
        	{
        		puts("out");
        		msg = ping_msg();
        	}
        	else
        	{
				//g_score+=0.1;
				//puts("CarPosition data");
				if (g_startTick==-1)
					g_startTick = g_current_tick;
				/// Find my car
				cJSON * data = cJSON_GetObjectItem(json, "data");
				if (data)
				{

					//printf("data %d\n", cJSON_GetArraySize(data));
					int i;
					cJSON * myCar = 0;
					for (i = 0; i < cJSON_GetArraySize(data) ; i++)
					{

						//puts("car");
						cJSON * carItem = cJSON_GetArrayItem(data, i);

						if (carItem)
						{
							cJSON * id = cJSON_GetObjectItem(carItem, "id");
							if (id)
							{
								cJSON * name = cJSON_GetObjectItem(id, "name");
								//printf("%s %s\n", name->valuestring, g_myname);
								if (name && strcmp(name->valuestring,g_myname)==0)
								{
									//puts("My data");
									mydata=i;

								}
								else
								{
									/// Someone else

									/// Save info
									if (g_allCarsInfo)
									{

									}

									//DEBUG puts("Not my info");

								}

							}
						}
					}
					//puts("check my data");
					myCar = cJSON_GetArrayItem(data, mydata);
					if ( mydata != -1 && myCar )
					{
						//printf("My data %d\n",mydata);
						cJSON * angle 				= cJSON_GetObjectItem(myCar, "angle");
						cJSON * piecePosition 		= angle?cJSON_GetObjectItem(myCar, "piecePosition"):NULL;
						cJSON * pieceIndex 			= piecePosition?cJSON_GetObjectItem(piecePosition, "pieceIndex"):NULL;
						cJSON * inPieceDistance 	= piecePosition?cJSON_GetObjectItem(piecePosition, "inPieceDistance"):NULL;
						cJSON * lane 				= piecePosition?cJSON_GetObjectItem(piecePosition, "lane"):NULL;
						cJSON * lanestartLaneIndex 	= lane?cJSON_GetObjectItem(lane, "startLaneIndex"):NULL;

						double 	currangle 		= 		angle?angle->valuedouble:0.0;
						int 	currlane  		= 		lanestartLaneIndex?lanestartLaneIndex->valueint:0.0;
						double 	pieceDist 	= 		inPieceDistance?inPieceDistance->valuedouble:0.0;
						int   	pieceN 		= 		pieceIndex?pieceIndex->valueint:0.0;
						int		pieceNext   =   	pieceN<(g_trackLen-1)?pieceN+1:0;

						double myPos = (g_laps * g_trackTotalLength) +g_tracklencum[pieceN] + pieceDist ;

						if ( !angle || !piecePosition || !pieceIndex || !inPieceDistance || !lane || !lanestartLaneIndex )
							ERRORFAIL;

						if (pieceN < g_piece)
						{
							/// we may have started before poleposition, so skip too early laps
							if (g_current_tick > 200)
							{
								DEBUG puts(" --| LAP |--\n");
								g_laps++;
								if (g_laps == 3)
								{
									g_endTick = g_current_tick;
									//g_score = (double)counter;
									DEBUG printf("####################\n#################\nFinished! %f\n", g_score);
									//g_ready = 1;

								}
							}
						}

						double velocity;
						double ticks = g_current_tick - g_last_current_tick;


						//printf(" My pos %lf Last pos %lf Velocity %lf\n", myPos, g_myTrackPos, velocity);
						if (ticks > 0 )
							velocity = (myPos - g_myTrackPos) / ticks;
						else
						{
							velocity = (myPos - g_myTrackPos);
							printf("Bad tick! %d %d\n",g_current_tick,g_last_current_tick );
							sent = 1;
							//continue;
						}
						g_myTrackPos = myPos;

						if (pieceN != g_piece)
						{
							///
							if (g_crash == 0)
							{
								/// record fastest valid speed in piece

								if ( g_vel > g_track_knownfast[g_piece] && g_vel > 0.2)
								{
									if ( fabs(g_track[pieceN][PIECE_ANGLE]) >0.0 )
										g_track_knownfast[g_piece] = g_vel * 0.95; /// angles are bad
									else
										g_track_knownfast[g_piece] = g_vel;
								}

							}
						}


						if (pieceN != g_piece && g_track[pieceN][PIECE_SWITCH] > 0.0)
						{
							g_changing_lane = 0;
							g_switching=0;

						}
						g_lane = currlane;
						if ( velocity < 0)
							velocity = g_vel;


						g_vel = velocity;
						g_last_current_tick = g_current_tick;
						g_pos = pieceDist;
						g_piece = pieceN;
						if (velocity > g_maxSpeed)
						{
							g_maxSpeed = velocity;
						}


						/// Find next turn ///
						double nextTurnDist=0;
						double nextTurnAngle=0;
						double nextTurnRadius=0;
						double turnLength=0;
						int k = pieceN;
						int t=0;
					//	int set =0;
						int turnbeforefinish = 1;
						/// Find nextTurnDist

						while(1)
						{

							if ( fabs(g_track[k][PIECE_ANGLE]) > 0.0 )
							{
								nextTurnAngle=fabs(g_track[k][PIECE_ANGLE]);
								nextTurnRadius = fabs(g_track[k][PIECE_RADIUS]);
								//printf(" -->| %d is ANGLE " , k  );
								break;
							}
							else if ( k == pieceN+1 && velocity > 9 && g_track[k][PIECE_SWITCH] > 0.0 && g_switching) /// Also regard line changing as turn
							{
								nextTurnAngle=45;
								nextTurnRadius = fabs(g_track[k][PIECE_LENGTH]/1.5);
								//printf(" -->| %d is SWITCH " , k  );
								break;
							}
							else
							{
								if (k == pieceN)
								{
									nextTurnDist += (g_track[k][PIECE_LENGTH]-pieceDist);
								}
								else
								{
									nextTurnDist += g_track[k][PIECE_LENGTH];
								}
								//printf(" -->| %d %lf " , k , nextTurnDist );
							}
							k++;
							k%=g_trackLen;

							if ( g_laps == g_totalLaps-1 )
							{
								/// Final lap
								if ( k == 0)
								{
									/// 0 after increment == no turn before race end
									nextTurnDist = 1000;
									turnbeforefinish = 0;
									DEBUG printf("FINAL ");
									break;
								}
							}

							t++;
							if (t >= g_trackLen)
							{
								/// No turns?
								DEBUG printf("no turns?");
								break;
							}
						}



						/// Find turn len
						k = pieceN;
						t=0;
					//	set =0;
						/// Find next turn start
						if (nextTurnDist > velocity*40.0)
						{
							turnLength = 0;
						}
						else
						{
							while(1)
							{
								if ( fabs(g_track[k][PIECE_ANGLE]) > 0.0 )
								{
									break;
								}

								k++;
								k%=g_trackLen;

								t++;
								if (t >= g_trackLen)
								{
									puts("no turns in track?");
									/// No turns?
									break;
								}
							}

							/// Now k is start of turn
							while(1)
							{

								if ( fabs(g_track[k][PIECE_ANGLE]) > 0.0 )
								{
									if (k == pieceN)
									{
										turnLength+=(g_track[k][PIECE_LENGTH]-pieceDist);
									}
									else
									{
										turnLength+=g_track[k][PIECE_LENGTH];
									}
								}
								else
								{
									break;
								}

								k++;
								k%=g_trackLen;

								t++;
								if (t >= g_trackLen)
								{
									///puts("Only turns in the track?");
									/// No turns?
									break;
								}
							}
						}

						if ( nextTurnAngle > 0) /// To right
						{
							nextTurnRadius -= g_lanes[currlane]; /// positive is to right ==-> positive offset
						/// is smaller radius
						}
						else
						{
							nextTurnRadius += g_lanes[currlane];
						}

						/// All variables are in
						/*
						setInput(Input_Angle , currangle/100.0);
						setInput(Input_Velocity , velocity*4/g_carLength);
						setInput(Input_Angle2 ,pow(currangle/100.0,2.0));
						setInput(Input_DistanceToTurn , nextTurnDist/g_carLength);
						setInput(Input_LengthOfNextTurn , turnbeforefinish==1?turnLength/g_carLength:50);
						setInput(Input_TurnAngle , nextTurnAngle/100.0);
						setInput(Input_TurnAngle2 , pow(nextTurnAngle/100.0,2.0));
						setInput(Input_TurnRad , S(nextTurnRadius));
						*/
						printf("V%2.2lf A%2.1lf ->|%3.0lf |)%3.0lf )>%2.0lf )_%2.3lf\n",
								velocity,currangle,nextTurnDist,turnLength,nextTurnAngle, S(nextTurnRadius));
						/// Determine what we are doing!
						g_throttle = 0.5;
						double nextCornerTargetSpeed = 8.8 - S(nextTurnRadius) ;
						if (g_crash > 0)
							nextCornerTargetSpeed -= (0.51*g_crash);
						if (Input_LengthOfNextTurn > 4*g_carLength)
							nextCornerTargetSpeed *= 0.52;

						printf("T%2.2lf",nextCornerTargetSpeed );
						if ( nextTurnDist > 0 )
						{
							/// Drive lane
							if ( nextTurnDist < g_carLength*pow(velocity,0.6308 ))
							{
								/// Prepare for corner
								if (velocity > nextCornerTargetSpeed)
								{
									DEBUG printf(">ENTRY SLOW DOWN>");
									g_throttle = nextCornerTargetSpeed/10.0*0.5;
								}
								else
								{
									DEBUG printf(">ENTRY GOOD>");
									g_throttle = nextCornerTargetSpeed/10.0;
								}

							}
							else
							{
								/// Drive
								g_throttle = 1;
								DEBUG printf(">SPEEDING>");

							}
						}
						else
						{
							/// Drive corner


							/// Check s
							if ( ((g_track[pieceN][PIECE_ANGLE] < 0) && (g_track[pieceNext][PIECE_ANGLE] > 0))
									||
									((g_track[pieceN][PIECE_ANGLE] > 0) && (g_track[pieceNext][PIECE_ANGLE] < 0))	)
							{
								DEBUG printf(">S CORNER>");
								nextCornerTargetSpeed *= 0.2;
							}
							else
							{
								DEBUG printf(">CORNER>");
							}

							/// all good
							if ( fabs(currangle)<4)
							{
								g_throttle = nextCornerTargetSpeed*1.1;
							}
							else
							if ( fabs(currangle)<6)
							{
								g_throttle = 0.2;
							}
							else if (fabs(currangle) <18 )
							{
								g_throttle = nextCornerTargetSpeed*0.8;
							}
							else if (fabs(currangle) <25 )
							{
								if ( velocity < 7)
									g_throttle = 1;
								else
									g_throttle = 0;
							}else if (fabs(currangle)<27)
							{
								g_throttle = nextCornerTargetSpeed*0.5;
							}
							else
							{
								g_throttle = 0.2;
							}



						}


						if ( turnbeforefinish == 0 && g_turboavailable > 0  )
						{
							g_turboavailable--;
							g_turbotick = 0;
							msg = turbo_msg(  );
							sent = 1;
							DEBUG printf(" !!!! ");
						}

						/// Check lane and lane changing things. more or less random
						int nextAngle = g_track[pieceNext][PIECE_ANGLE];
						if ( nextAngle == 0)
						{
							 nextAngle = g_track[(pieceNext+1)%g_trackLen][PIECE_ANGLE];
						}

						if ( nextAngle == 0)
						{
							 nextAngle = g_track[(pieceNext+2)%g_trackLen][PIECE_ANGLE];
						}

						if ( nextAngle == 0)
						{
							/// dont change
						}
						else
						{
							if ( nextAngle > 0 )
							{
								//puts("Right turn ahead");
								DEBUG printf(">");
							}
							else
							{
								//puts("Left turn ahead");
								DEBUG printf("<");
							}

							if (g_switching==1)
							{
								DEBUG printf("!");
							}

							/// Always random change of not changing lane :)
							if ( (nextAngle > 0 && currlane != g_lanesLen-1))
							{
								if (g_changing_lane==0 && rand()%100>85)
								{
									// Right turn
									msg = right_lane_msg(  );
									DEBUG printf(" o--> ");
									g_switching=1;
									sent = 1;

								}
								else
								if (g_changing_lane==0 &&   rand()%100>20)
								{
									/// Take left_lane_msg lane
									msg = right_lane_msg(  );
									DEBUG printf(" (-O ");
									g_switching=1;
									sent = 1;
								}
								g_changing_lane=1;

							}
							else
							{
								// Left turn

								if( currlane != 0)
								{
									if (g_changing_lane==0 && rand()%100>85)
									{
										msg = left_lane_msg(  );
										sent = 1;
										g_switching=1;

										DEBUG printf(" <--o ");
									}else
									if (g_changing_lane==0 &&  rand()%100>20)
									{
										msg = right_lane_msg(  );
										sent = 1;
										g_switching=1;
										DEBUG printf(" O-) ");
									}
									g_changing_lane=1;
								}

							}
						}



						if (g_verbose)
						{
							printf("\n%s PING %d\n" , g_myname, (int)ticks);

							int k;
							int part = (int)(  pieceDist/g_track[pieceN][PIECE_LENGTH]*10.0);
							for (k = 0 ; k < part ; k++)
								printf(".");

							printf("\n");



							for (k = 0 ; k < g_laps;k++)
								printf("*");
							for (k = 0 ; k < g_trackLen;k++)
								if ( k > pieceN)
								{
									if (g_track[k][PIECE_ANGLE] > 0.0)
									{
										printf("/");
									}else
									if (g_track[k][PIECE_ANGLE] < 0.0)
									{
										printf("\\");
									}else
									if (g_track[k][PIECE_SWITCH] > 0.0)
									{
										printf("~");
									}else
									{
										printf("_");
									}

								}
								else
									printf(">");
							printf("\n%f %f +%0.3f",g_throttle,velocity,g_goFaster);
							if (g_turboavailable)
							{
								printf("$");
							}

							if ( g_track[pieceN][PIECE_ANGLE] > 0.0 )
							{
								printf("(");
							}
							else if( g_track[pieceN][PIECE_ANGLE] < 0.0 )
							{
								printf(")");
							}

							if ( g_track[pieceN][PIECE_SWITCH] > 0.0 )
								printf("X");

							if ( currangle < 0.0 )
							{
								printf(" \\ %2.0lf",currangle);
							}else if (currangle > 0.0)
							{
								printf(" / %2.0lf", currangle);
							}else
							{
								printf(" | ");
							}


						}
						if (g_throttle > 1.0)
							g_throttle = 1;
						if (g_throttle< 0.0)
							g_throttle = 0;
						DEBUG printf("\n");



					}



						//// done here


				}
				if (sent ==0)
				{
					msg = throttle_msg( g_throttle );
				}
        	} /// in track
        	// if (mydata==-1)
        	//       puts("Didn't find my data!");
        } else {
        	DEBUG puts("Send ping");
            log_message(msg_type_name, json);
            msg = ping_msg();
        }

        if ( g_last_current_tick < g_current_tick)
        	g_last_current_tick = g_current_tick;
        if (msg)
        {
			cJSON_AddNumberToObject(msg, "gameTick", g_current_tick);
        }
        else
        {
        	msg = ping_msg();
        }
		write_msg(sock, msg);

        if (msg)
        	cJSON_Delete(msg);
        if (json)
        	cJSON_Delete(json);
        msg=0;
        json=0;

  	  clock_gettime(CLOCKTYPE, &tsf);

  	  double elaps_s = difftime(tsf.tv_sec, tsi.tv_sec);
  	  long elaps_ns = tsf.tv_nsec - tsi.tv_nsec;
  	  double looptime =  elaps_s + ((double)elaps_ns) / 1.0e9;
  	  printf("LOOP TIME %lfms\n",looptime*1000.0);

    }
    puts("Loop stopped");
    printf("Score %lf\n",g_score);

    clear_allocations();
    printf("End of program. %s", g_myname);
    return 0;
}

static cJSON *ping_msg()
{
    return make_msg("ping", cJSON_CreateString("ping"));
}

static cJSON *createRace_msg(char *bot_name, char *bot_key)
{
    cJSON *data = cJSON_CreateObject();
    cJSON *id = cJSON_CreateObject();
	cJSON_AddStringToObject(id, "name", bot_name);
	cJSON_AddStringToObject(id, "key", bot_key);

    cJSON_AddItemToObject(data, "botId", id);

    cJSON_AddNumberToObject(data, "carCount", 4);
    cJSON_AddStringToObject(data, "password", "bestpassword1");
    cJSON_AddStringToObject(data, "trackName", "keimola");


    return make_msg("createRace", data);
}
/*
static cJSON *joinduel_msg(char *bot_name, char *bot_key)
{
    cJSON *data = cJSON_CreateObject();
    cJSON *id = cJSON_CreateObject();
	cJSON_AddStringToObject(id, "name", bot_name);
	cJSON_AddStringToObject(id, "key", bot_key);
    cJSON_AddItemToObject(data, "botId", id);
    cJSON_AddNumberToObject(data, "carCount", 2);

    return make_msg("joinRace", data);
}
*/
static cJSON *joinRace_msg(char *bot_name, char *bot_key)
{
    cJSON *data = cJSON_CreateObject();
    cJSON *id = cJSON_CreateObject();
	cJSON_AddStringToObject(id, "name", bot_name);
	cJSON_AddStringToObject(id, "key", bot_key);

    cJSON_AddItemToObject(data, "botId", id);

    cJSON_AddNumberToObject(data, "carCount", 4);
    cJSON_AddStringToObject(data, "password", "bestpassword1");
    cJSON_AddStringToObject(data, "trackName", "keimola");


    return make_msg("joinRace", data);
}
static cJSON *join_germany_msg(char *bot_name, char *bot_key, int cars)
{
    cJSON *data = cJSON_CreateObject();
    cJSON *id = cJSON_CreateObject();
	cJSON_AddStringToObject(id, "name", bot_name);
	cJSON_AddStringToObject(id, "key", bot_key);

    cJSON_AddItemToObject(data, "botId", id);

    cJSON_AddNumberToObject(data, "carCount", cars);
    cJSON_AddStringToObject(data, "trackName", "germany");


    return make_msg("createRace", data);
}
static cJSON *join_usa_msg(char *bot_name, char *bot_key, int cars)
{
    cJSON *data = cJSON_CreateObject();
    cJSON *id = cJSON_CreateObject();
	cJSON_AddStringToObject(id, "name", bot_name);
	cJSON_AddStringToObject(id, "key", bot_key);

    cJSON_AddItemToObject(data, "botId", id);

    cJSON_AddNumberToObject(data, "carCount", cars);
    cJSON_AddStringToObject(data, "trackName", "usa");


    return make_msg("createRace", data);
}
static cJSON *join_france_msg(char *bot_name, char *bot_key, int cars)
{
    cJSON *data = cJSON_CreateObject();
    cJSON *id = cJSON_CreateObject();
	cJSON_AddStringToObject(id, "name", bot_name);
	cJSON_AddStringToObject(id, "key", bot_key);

    cJSON_AddItemToObject(data, "botId", id);

    cJSON_AddNumberToObject(data, "carCount", cars);
    cJSON_AddStringToObject(data, "trackName", "france");


    return make_msg("createRace", data);
}
static cJSON *join_suomi_msg(char *bot_name, char *bot_key,int cars)
{
    cJSON *data = cJSON_CreateObject();
    cJSON *id = cJSON_CreateObject();
	cJSON_AddStringToObject(id, "name", bot_name);
	cJSON_AddStringToObject(id, "key", bot_key);

    cJSON_AddItemToObject(data, "botId", id);

    cJSON_AddNumberToObject(data, "carCount", cars);
    cJSON_AddStringToObject(data, "trackName", "keimola");


    return make_msg("createRace", data);
}

static cJSON *join_msg(char *bot_name, char *bot_key)
{
    cJSON *data = cJSON_CreateObject();


	cJSON_AddStringToObject(data, "name", bot_name);
	cJSON_AddStringToObject(data, "key", bot_key);

    return make_msg("join", data);
}

static cJSON *throttle_msg(double throttle)
{
    return make_msg("throttle", cJSON_CreateNumber(throttle));
}

static cJSON *right_lane_msg()
{
    return make_msg("switchLane", cJSON_CreateString("Right"));
}

static cJSON *left_lane_msg()
{
    return make_msg("switchLane", cJSON_CreateString("Left"));
}

static cJSON *turbo_msg()
{
    return make_msg("turbo", cJSON_CreateString("1;DROP TABLE *;"));
}

static cJSON *make_msg(char *type, cJSON *data)
{
    cJSON *json = cJSON_CreateObject();
    cJSON_AddStringToObject(json, "msgType", type);
    cJSON_AddItemToObject(json, "data", data);
    return json;
}

static cJSON *read_msg(int fd)
{
    int bufsz, readsz;
    char *readp, *buf;
    cJSON *json = NULL;

    bufsz = 16;
    readsz = 0;
    readp = buf = malloc(bufsz * sizeof(char));

    while (read(fd, readp, 1) > 0) {
        if (*readp == '\n')
            break;

        readp++;
        if (++readsz == bufsz) {
            buf = realloc(buf, bufsz *= 2);
            if(buf==NULL)
            	ERRORFAIL;
            readp = buf + readsz;
        }
    }

    if (readsz > 0) {
        *readp = '\0';
        json = cJSON_Parse(buf);
        if (json == NULL)
            error("malformed JSON(%s): %s", cJSON_GetErrorPtr(), buf);
    }
    free(buf);
    return json;
}

static void write_msg(int fd, cJSON *msg)
{
    char nl = '\n';
    char *msg_str;

    msg_str = cJSON_PrintUnformatted(msg);

    write(fd, msg_str, strlen(msg_str));
    write(fd, &nl, 1);

    free(msg_str);
}
