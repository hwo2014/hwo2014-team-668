/// Note to self: this code is horrible

#include <errno.h>
#include <netdb.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include <stdlib.h>
#include "cJSON.h"
#include <math.h>
#include <pthread.h>

static cJSON *ping_msg();
static cJSON *join_msg(char *bot_name, char *bot_key);
//static cJSON *joinduel_msg(char *bot_name, char *bot_key);
static cJSON *join_germany_msg(char *bot_name, char *bot_key, int cars);
static cJSON *join_usa_msg(char *bot_name, char *bot_key, int cars);
static cJSON *join_suomi_msg(char *bot_name, char *bot_key, int cars);
static cJSON *join_france_msg(char *bot_name, char *bot_key, int cars);
static cJSON *throttle_msg(double throttle);
static cJSON *right_lane_msg();
static cJSON *left_lane_msg();
static cJSON *turbo_msg();
static cJSON *make_msg(char *type, cJSON *msg);
static cJSON *read_msg(int fd);
static void write_msg(int fd, cJSON *msg);
static cJSON *createRace_msg(char *bot_name, char *bot_key);
static cJSON *joinRace_msg(char *bot_name, char *bot_key);
static cJSON *join_race_msg(char *race , char *bot_name, char *bot_key, int cars);
int countlines(const char* filename);
double getOutput();
void setInput(int input, double value);
/*
#define		Input_current_piece_length 0
#define		Input_current_piece_angle2 1
#define		Input_current_piece_angle 2
#define		Input_current_piece_radius 3
//#define		Input_current_piece_switch 4
#define		Input_next_piece_length 4
#define		Input_next_piece_angle2 5
#define		Input_next_piece_angle 6
#define		Input_next_piece_radius 7
//#define		Input_next_piece_switch 9
#define		Input_angle  8
#define		Input_angle2 9
#define		Input_current_pos 10
//#define		Input_current_throttle 11
//#define		Input_current_lane 14
//#define		Input_max_lane 15
#define		Input_current_lane_dist 11
//#define		Input_car_width 17
//#define		Input_car_length 18
//#define		Input_car_flag 19
#define		Input_car_velocity 12

#define		Input_next_turn_dist 13
#define		Input_next_turn_a 14
#define		Input_next_turn_r 15
*/

#define S(r) ((fabs(r)>0.0)?(atan( 1.0 / (sqrt(2.0*r-1.0)) )*180.0/ 3.14159265):(0.0))

#define 	Input_Velocity 					0
#define		Input_Angle						1
#define		Input_Angle2					2
#define		Input_AngleDiff					3
#define		Input_AngleDiff2				4
#define		Input_DistanceToTurn			5
#define		Input_LengthOfNextTurn			6
#define		Input_TurnAngle					7
#define		Input_TurnAngle2				8
#define		Input_TurnRad					9
#define     Input_M1_AngleDiff  			10
#define     Input_M1_AngleDiff2				11
#define 	Input_M1_Velocity				12

#define     Input_M2_AngleDiff				13
#define     Input_M2_AngleDiff2				14
#define 	Input_M2_Velocity				15
#define     Input_M3_AngleDiff				16
#define     Input_M3_AngleDiff2				17
#define 	Input_M3_Velocity				18

#define		Input_Count						19



#define		Variable_TakeInnerLane			0
#define		Variable_AntiSlipAccMinVelocity 1
#define		Variable_AntiSlipAccLength		2
#define		Variable_AntiSlipMinAngle		3
#define		Variable_SlowDownLength			4
#define		Variable_StraightLaneAntiSlip   5
#define		Variable_UseKnownFast			6
#define		Variable_MinTurboLenghtPerTick  7
#define		Variable_Tactics				8
#define		Variable_EnterCornerSpeedLimit  9
#define		Variable_SteepAngleValue        9
#define		Variable_EnterCornerSpeedDist   10
#define		Variable_Count					11


#define 		PIECE_LENGTH 0
#define 		PIECE_ANGLE  1
#define 		PIECE_RADIUS 2
#define 		PIECE_SWITCH 3
#define 		PIECE_CARS   4
#define 		PIECE_COUNT  5

#define			RANGE 1.0
int 			g_generation;

double** 		g_track;
double* 		g_tracklencum;
double* 		g_track_knownfast;
double* 		g_track_speedlimit;

char**			g_carNames;

int				g_trackLen;
char			g_myname[32];
int 			g_trackInitialized;
int				g_inTrack;
int             g_hasScore;
int				g_switching;

int				g_lanesLen;
double*			g_lanes;

int 			g_lastPiece;

double			g_carLength;
double			g_carWidth;

double			g_carFlagpos;

int				g_weightsLen;
double*			g_weights;
double*			g_variables;
//int				g_variablesLen;
double*			g_inputs;
double			g_output;
double			g_lastAngle;
double			g_throttle;

int				g_current_tick;
int				g_last_current_tick;
int				g_startTick;
int				g_endTick;
double			g_goFaster;

int				g_abort;

double			g_pos;
int				g_piece;
int				g_laps;
double			g_score;

int				g_crash;
int				g_ready;
int				g_mode;
double 			g_vel;
double			g_maxSpeed;
double			g_bestLap;
#define carInfo_pieceN 0
#define carInfo_pieceD 1
#define carInfo_crashed 2
#define carInfo_totalDistance 3
#define carInfo_totalDistancePrev 4
#define carInfo_velocity 5
#define carInfo_lane 6
#define carInfo_active 7
#define carInfo_angle 8
#define carInfo_crash 9
#define carInfo_count 10
double**			g_allCarsInfo;
int					g_allCarsInfoN;

double				g_minAngle;
double				g_maxAngle;


#define type_random 1
#define type_mutate 2
#define type_crossover 3
#define type_static 4
int				g_type;

int 			g_totalLaps;

int				g_lane;
int				g_changing_lane;

int			g_generationSize;
int			g_generationBest;

double		g_trackTotalLength;
double		g_myTrackPos;

int 		g_turboavailable;
int			g_turbotick;

int			g_slowdown;
int			g_antiskip;

int 		g_verbose;

#define DEBUG if(g_verbose==1)



void clear_trackInfo()
{
	DEBUG puts("Cleanup track");
	int i;

	if ( g_track )
	{

		for ( i = 0; i < g_trackLen ; i++ )
		{
			if (g_track[i])
			{
				free (g_track[i]);
			}
		}
		free(g_track);
	}
	g_track = 0;


	if (g_lanes)
	{
		free (g_lanes);
	}
	g_lanes = 0;

	if(g_tracklencum)
	{
		free (g_tracklencum);
	}
	g_tracklencum = 0;

	if (g_track_knownfast)
	{
		free(g_track_knownfast);
	}
	g_track_knownfast = 0;

	if (g_track_speedlimit)
	{
		free(g_track_speedlimit);
	}
	g_track_speedlimit = 0;
	DEBUG puts("Cleanup done");
}


void initCarInfo(int n)
{

	printf(" init car info %d\n", n);
	if ( g_allCarsInfo)
	{
		puts("error alloc car info already allocated");
		exit(2);
	}
	else
	{
		g_carNames = (char**)malloc(sizeof(char*)*n);
		g_allCarsInfoN = n;
		g_allCarsInfo = (double**)malloc(sizeof(double*)*n);
		if ( !g_allCarsInfo)
		{
			puts("error alloc car info");
			exit(2);
		}
		int i;
		printf(" init car info %d all cars\n", n);
		for (i = 0; i < g_allCarsInfoN ; i++)
		{
			g_carNames[i] = (char*)malloc(sizeof(char)*32);
			memset( g_carNames[i] , 0 , 32);
			g_allCarsInfo[i] = (double*)malloc(sizeof(double)*carInfo_count);
			if ( g_allCarsInfo[i] == 0)
			{
				puts("error sub alloc car info");
				exit(2);
			}
			int k;
			for (k = 0; k < carInfo_count ; k++)
			{
				g_allCarsInfo[i][k] = 0;
			}

		}
	}
	printf(" init car info %d ok\n", n);

}

void clearCarInfo()
{

	DEBUG puts("Clear car info");
	if (  g_allCarsInfo )
	{
		int i;
		for (i = 0; i < g_allCarsInfoN ; i++)
		{
			if ( g_allCarsInfo[i]) free (g_allCarsInfo[i]);
			if ( g_carNames[i]) free(g_carNames[i]);
		}
		free(g_allCarsInfo);
		free(g_carNames);
	}
	g_carNames =0;
	g_allCarsInfo = 0;
	DEBUG puts("Clear car info done");

}

void clear_allocations()
{
	DEBUG puts("Cleanup memory");

	if (g_weights)
	{
		free (g_weights);
	}
	g_weights = 0;
	if ( g_inputs)
	{
		free (g_inputs);
	}

	if ( g_variables)
	{
		free (g_variables);
	}
	DEBUG puts("Cleanup memory done");
	g_weights = 0;
	g_variables = 0;
	g_inputs = 0;
	clear_trackInfo();
	clearCarInfo();
}
#define ERRORFAIL { printf("ERROR %d\n", __LINE__);exit(1);}


/// Util
double getRandomdouble(double min, double max)
{
	double f = (double)rand() / RAND_MAX;
	return min + f * (max - min);
}

/// GA

void initializeGA()
{

	g_inputs = (double*)malloc( sizeof(double)*Input_Count );
	if (!g_inputs) ERRORFAIL;

	g_weights = (double*)malloc( sizeof(double)* g_weightsLen);
	if (!g_weights) ERRORFAIL;
	int i;
	for (i = 0; i <  g_weightsLen ; i++)
	{
		g_weights[i] = 0.0;
	}

	g_variables = (double*)malloc(sizeof(double)*Variable_Count);

}

void randomize()
{
	g_type = type_random;
	int ok = 9000;
	while ( ok > 3 )
	{

		if ( !g_weights )
		{
			puts("GA no initialized! Cannot randomize\n");
			ERRORFAIL;
		}

		int p = 0;
		int i,j;
		for ( i = 0 ; i < Input_Count; i++ )
		{
			g_weights[p] = getRandomdouble(-RANGE,RANGE);
			printf("%d\t: %f\n", i,g_weights[p]);
			p++;
		}
		for ( i = 0 ; i < Input_Count; i++ )
		{
			for (j = i ; j< Input_Count ; j++)
			{
				g_weights[p] = getRandomdouble(-RANGE,RANGE);
				printf("%d -> %d\t: %f\n", i,j,g_weights[p]);
				p++;
			}
		}
		for ( i = 0 ; i < Variable_Count; i++ )
		{
			g_variables[i] = getRandomdouble(-RANGE,RANGE);
		}

	#define PRECHECK 1

		ok = 0; /// count bad things here

		if ( PRECHECK == 1 )
		{

			ok = 0;

			setInput(Input_M3_AngleDiff , 0);
			setInput(Input_M3_AngleDiff2 , 0);
			setInput(Input_M3_Velocity ,  0);



			setInput(Input_M2_AngleDiff , 0);
			setInput(Input_M2_AngleDiff2 , 0);
			setInput(Input_M2_Velocity , 0);


			setInput(Input_M1_AngleDiff ,  0);
			setInput(Input_M1_AngleDiff2 , 0);
			setInput(Input_M1_Velocity , 0);



			//Clear start at keimola
			setInput(Input_Velocity , 0*4/g_carLength);
			setInput(Input_Angle , 0/100.0);
			setInput(Input_Angle2 ,pow(0,2.0)/100.0);
			setInput(Input_AngleDiff ,pow(0,2.0)/10.0);
			setInput(Input_AngleDiff2 ,pow(0,2.0)/10.0);
			setInput(Input_DistanceToTurn , 200/g_carLength);
			setInput(Input_LengthOfNextTurn , 120/g_carLength);
			setInput(Input_TurnAngle , 0.45);
			setInput(Input_TurnAngle2 , pow(0.45,2.0));
			setInput(Input_TurnRad , S(90.0));

			if (getOutput() < 0.9)
				ok+=5;


			setInput(Input_M3_AngleDiff , 0);
			setInput(Input_M3_AngleDiff2 , 0);
			setInput(Input_M3_Velocity ,  6.8*4/g_carLength);


			setInput(Input_M2_AngleDiff , 0);
			setInput(Input_M2_AngleDiff2 , 0);
			setInput(Input_M2_Velocity , 6.8*4/g_carLength);


			setInput(Input_M1_AngleDiff ,  0);
			setInput(Input_M1_AngleDiff2 , 0);
			setInput(Input_M1_Velocity , 6.7*4/g_carLength);



			//Clear start at keimola
			setInput(Input_Velocity , 7*4/g_carLength);
			setInput(Input_Angle , 0/100.0);
			setInput(Input_Angle2 ,pow(0,2.0)/100.0);
			setInput(Input_AngleDiff ,pow(0,2.0)/10.0);
			setInput(Input_AngleDiff2 ,pow(0,2.0)/10.0);
			setInput(Input_DistanceToTurn , 50/g_carLength);
			setInput(Input_LengthOfNextTurn , 150/g_carLength);
			setInput(Input_TurnAngle , 0.45);
			setInput(Input_TurnAngle2 , pow(0.45,2.0));
			setInput(Input_TurnRad , S(90.0));

			if (getOutput() > 0.6)
				ok++;

			if (getOutput() < 0.1)
				ok++;

			setInput(Input_M3_AngleDiff ,0.0/10.0);
			setInput(Input_M3_AngleDiff2 , pow(0,2.0)/10.0);
			setInput(Input_M3_Velocity , 7*4/g_carLength);


			setInput(Input_M2_AngleDiff , 0.0/10.0);
			setInput(Input_M2_AngleDiff2 , pow(0,2.0)/10.0);
			setInput(Input_M2_Velocity , 7*4/g_carLength);


			setInput(Input_M1_AngleDiff ,  0.0/10.0);
			setInput(Input_M1_AngleDiff2 , pow(0,2.0)/10.0);
			setInput(Input_M1_Velocity , 7*4/g_carLength);



			//Clear start at keimola
			setInput(Input_Velocity , 7*4/g_carLength);
			setInput(Input_Angle , 0/100.0);
			setInput(Input_Angle2 ,pow(0,2.0)/100.0);
			setInput(Input_AngleDiff ,0/10.0);
			setInput(Input_AngleDiff2 ,pow(0,2.0)/10.0);
			setInput(Input_DistanceToTurn , 50/g_carLength);
			setInput(Input_LengthOfNextTurn , 150/g_carLength);
			setInput(Input_TurnAngle , 0.45);
			setInput(Input_TurnAngle2 , pow(0.45,2.0));
			setInput(Input_TurnRad , S(90.0));

			if (getOutput() > 0.6)
				ok++;

			if (getOutput() < 0.1)
				ok++;


			setInput(Input_M3_AngleDiff ,2.0/10.0);
			setInput(Input_M3_AngleDiff2 , pow(2.0,2.0)/10.0);
			setInput(Input_M3_Velocity , 7*4/g_carLength);


			setInput(Input_M2_AngleDiff , 1.8/10.0);
			setInput(Input_M2_AngleDiff2 , pow(1.8,2.0)/10.0);
			setInput(Input_M2_Velocity , 7*4/g_carLength);


			setInput(Input_M1_AngleDiff ,  1.8/10.0);
			setInput(Input_M1_AngleDiff2 , pow(1.8,2.0)/10.0);
			setInput(Input_M1_Velocity , 7*4/g_carLength);



			//Clear start at keimola
			setInput(Input_Velocity , 8*4/g_carLength);
			setInput(Input_Angle , 40.0/100.0);
			setInput(Input_Angle2 ,pow(40.0,2.0)/100.0);
			setInput(Input_AngleDiff ,5/10.0);
			setInput(Input_AngleDiff2 ,pow(5,2.0)/10.0);
			setInput(Input_DistanceToTurn , 0/g_carLength);
			setInput(Input_LengthOfNextTurn , 150/g_carLength);
			setInput(Input_TurnAngle , 0.45);
			setInput(Input_TurnAngle2 , pow(0.45,2.0));
			setInput(Input_TurnRad , S(90.0));

			if (getOutput() > 0.5)
				ok++;

			setInput(Input_M3_AngleDiff ,-2.0/10.0);
			setInput(Input_M3_AngleDiff2 , pow(-2.0,2.0)/10.0);
			setInput(Input_M3_Velocity , 7*4/g_carLength);


			setInput(Input_M2_AngleDiff , -1.8/10.0);
			setInput(Input_M2_AngleDiff2 , pow(-1.8,2.0)/10.0);
			setInput(Input_M2_Velocity , 7*4/g_carLength);


			setInput(Input_M1_AngleDiff ,  -1.8/10.0);
			setInput(Input_M1_AngleDiff2 , pow(-1.8,2.0)/10.0);
			setInput(Input_M1_Velocity , 7*4/g_carLength);



			//Clear start at keimola
			setInput(Input_Velocity , 8*4/g_carLength);
			setInput(Input_Angle , -40.0/100.0);
			setInput(Input_Angle2 ,pow(-40.0,2.0)/100.0);
			setInput(Input_AngleDiff ,-5/10.0);
			setInput(Input_AngleDiff2 ,pow(-5,2.0)/10.0);
			setInput(Input_DistanceToTurn , 0/g_carLength);
			setInput(Input_LengthOfNextTurn , 150/g_carLength);
			setInput(Input_TurnAngle , -0.45);
			setInput(Input_TurnAngle2 , pow(-0.45,2.0));
			setInput(Input_TurnRad , S(90.0));

			if (getOutput() > 0.5)
				ok++;


			sleep(0);
			printf("Random FAULT %d\n",ok);

		}/// End precheck



	}
	DEBUG puts("Random car ok");



}

int file_exists(const char * filename)
{
	FILE * file= fopen(filename, "r");
    if (file )
    {
        fclose(file);
        return 1;
    }
    return 0;
}
//static pthread_mutex_t locklock = PTHREAD_MUTEX_INITIALIZER;
void getWriteLock()
{

	sleep(0);
	DEBUG puts("Lock");
	/// Not good for this if many
	int w = 1;
//	pthread_mutex_lock(&locklock);  // lock the critical section
	while (file_exists("GA.lock") == 1)
	{
	//	pthread_mutex_unlock(&locklock);  // lock the critical section

		puts("FILE BUSY");
		sleep(1);
		w++;
		if ( w == 9 )
			break;
		//sleep(1+(int)(rand()%3));
	//	pthread_mutex_lock(&locklock);  // lock the critical section

	}

	FILE *fp;
	fp=fopen("GA.lock", "w");
	if ( fp)
	{
		fprintf(fp, "Locked");
		fclose(fp);
	}
//	pthread_mutex_unlock(&locklock);  // lock the critical section

}

void freeWriteLock()
{

	int status;
	int i;
	for (i = 0;i<10;i++)
	{
		DEBUG puts("Remove lock");
	//	pthread_mutex_lock(&locklock);  // lock the critical section
		status = remove("GA.lock");
	//	pthread_mutex_unlock(&locklock);  // lock the critical section
	   if( status == 0 )
	   {
		   DEBUG printf("lockfile deleted successfully.\n");
		  return;

	   }
	   else
	   {
		   DEBUG puts("Unable to delete the file\n");
	   }

	   sleep(2);


	}
	DEBUG puts("Unable to delete the lockfile! Lock did not work\n");


}

void writeToLogFile()
{

	printf("Write log...");


	char filename2[60];
	sprintf(filename2,"logs/ALL_.txt");
	FILE * file= fopen(filename2, "a+");



	if (file)
	{

		//double s = 0.0+g_score;
		fprintf(file, "%s,%d,%d,%d,%d,%d,%lf,",g_myname, g_hasScore,g_generation ,g_crash,g_laps,g_current_tick,g_score);



		DEBUG printf("Score %lf \n",g_score);
		int i ;
		for (i = 0; i < Variable_Count; i++ )
		{
			fprintf(file,"%lf,", g_variables[i]);
		}


		for (i = 0; i < g_weightsLen ; i++)
		{
			fprintf(file,"%lf", g_weights[i]);


			if ( i < g_weightsLen-1)
			{
				fprintf(file,",");

			}
		}
		fprintf(file,"\n");
		printf("log ok\n");
		fclose(file);

	}
	else
	{
		ERRORFAIL;
	}


}

void writeToFile()
{
	DEBUG puts("Write genome to file");
	getWriteLock();
	DEBUG puts("Acquired lock");

	unsigned int uID = rand()%999999;

	char filename2[60];
	sprintf(filename2,"logs/_LOGS_gene_%d_%u_%d.txt", g_generation+1, (unsigned)time(NULL) , uID);
	FILE * file_log= fopen(filename2, "w+");

	char filename[60];
	sprintf(filename,"gene_%d.txt", g_generation+1);
	FILE * file= fopen(filename, "a");

	if (file)
	{
		//double s = 0.0+g_score;
		fprintf(file, "%lf ", g_score);

		if ( file_log) fprintf(file_log, "%lf ", g_score);

		DEBUG printf("Score %lf \n",g_score);
		int i ;
		for (i = 0; i < Variable_Count; i++ )
		{
			fprintf(file,"%lf ", g_variables[i]);
			if ( file_log) fprintf(file_log, "%lf ", g_variables[i]);
		}


		for (i = 0; i < g_weightsLen ; i++)
		{
			fprintf(file,"%lf", g_weights[i]);
			if ( file_log) fprintf(file_log, "%lf", g_weights[i]);

			if ( i < g_weightsLen-1)
			{
				fprintf(file," ");
				if ( file_log) fprintf(file_log, " ");
			}
		}
		fprintf(file,"\n");
		DEBUG puts("Close file");
		fclose(file);
		if ( file_log) fclose(file_log);
	}
	else
	{
		ERRORFAIL;
	}

	DEBUG puts("Free lock");
	freeWriteLock();
	DEBUG puts("Writing compelete");
}

void readFromFile(const char* filename,int n,double* variables ,double* weights)
{
	DEBUG printf("Read config %d from file %s\n", n, filename);
	FILE *myfile;

	if (countlines(filename) < n)
	{
		printf("Cannot read %d, only %d lines!\n", n , countlines(filename));
		ERRORFAIL;
	}

	myfile= fopen(filename,"r");
	if (myfile != NULL)
	{
		char ch;
		int i=0;
		while(n>0 && 1)
		{
		  ch = fgetc(myfile);
		  if(ch == '\n')
		  {
			i++;
			if (i == n)
				break;
		  }
		}
		/// now at line n
		double score;

		fscanf(myfile,"%lf",&score);
		DEBUG printf("score %lf\n",score);
		for (i = 0; i < Variable_Count; i++ )
		{

			fscanf(myfile,"%lf",&variables[i]);
			DEBUG printf("V%d %f\n",i,variables[i]);
		}
		for (i = 0; i < g_weightsLen; i++ )
		{
			fscanf(myfile,"%lf",&weights[i]);
			DEBUG printf("W%d %f\n",i,weights[i]);
			//printf("\n");
		}
		fclose(myfile);
		DEBUG puts("Reading complete");
	}
	else
	{
		ERRORFAIL;
	}


}

void setupBot(int t)
{
	DEBUG printf("setupBot\n");
	g_type = type_static;
	char filename[60];
	sprintf(filename,"VARIABLES");
	int lines = countlines(filename)/10;
	if (t > lines)
	{
		printf("ERROR %d > %d\n", t ,lines);
		ERRORFAIL;
	}
	DEBUG printf("Run %d\n", t);

//	int r = rand() % lines;
	DEBUG printf("Select %d\n",t );
	double w[g_weightsLen];
	double v[Variable_Count];
	readFromFile(filename,t,&v[0],&w[0]);
	int i;
	printf("Set params %d %d\n",Variable_Count, g_weightsLen);
	for (i = 0; i < Variable_Count; i++)
	{
		g_variables[i] = v[i];
		printf("V%d %lf\n", i , g_variables[i]);
	}
	for (i = 0; i < g_weightsLen; i++)
	{
		g_weights[i] = w[i];
		printf("W%d %lf\n", i , g_weights[i]);
	}

	printf("setupBot Ready\n");

}

int getParent(int n)
{
	int group = rand()%5;
	DEBUG puts("Get parent");
	int m;
	if (group > 1)
	{
		DEBUG puts("Alpha");
		/// best 5%
		m = n / 50.0;
	}
	else if (group == 1)
	{
		DEBUG puts("Beta");
		/// best 50%
		m = n / 2.0;
	}
	else
	{
		DEBUG puts("Any");
		/// all
		m = n;
	}
	int r=0 ;
	if (m>0)
		r= rand()%m;
	DEBUG printf("%d\n",r);
	return r;

}

void setWeights()
{
	DEBUG printf("Read weights\n");
	if ( !g_weights )
	{
		puts("GA no initialized! Cannot set weights\n");
		ERRORFAIL;
	}

    if (g_generation == 0)
    {
    	DEBUG puts("Generation 0 -> random");
    	randomize();
    	/*
    	double w[g_weightsLen];
    	char filename[60];
    	sprintf(filename,"gene_1.txt");
    	readFromFile(filename,12,&w[0]);
    	int p;
    	int i,j;
		for ( i = 0 ; i < Input_count; i++ )
		{
			for (j = i ; j< Input_count ; j++)
			{
				g_weights[p] = w[p];
				p++;
			}
		}
		*/

    	char bot_name[32];
		sprintf(bot_name,"01%02dantikamel_",(int)(rand()%99) );
		memset(g_myname,'\0',32);
		strcpy(g_myname,bot_name );
		printf("Join race %s\n",g_myname);
		strcpy(g_myname,bot_name );

    	return;
    }


	int operation = rand()%14;


	//double w[g_weightsLen];

	//readFromFile(filename,1,&w[0]);
	//readFromFile(filename,2,&w[0]);

	int i,j;
	if ( operation > 11) /// 13 12 11
	{
		g_type = type_static;
		DEBUG puts("No mutation");
		int parent =getParent(g_generationBest);
		double w[g_weightsLen];
		double v[Variable_Count];
		char filename[60];
		sprintf(filename,"gene_%d.txt.sort", g_generation);

    	char bot_name[32];
		sprintf(bot_name,"%02d%02dS_%s_",(int)(rand()%99),g_generation,g_myname );
		memset(g_myname,'\0',32);
		strcpy(g_myname,bot_name );
		printf("Join race %s\n",g_myname);
		strcpy(g_myname,bot_name );

		readFromFile(filename,parent,&v[0],&w[0]);

		for ( i = 0 ; i < Variable_Count; i++ )
			g_variables[i]  = v[i]+getRandomdouble(-0.01,0.01);
		int p=0;
		for ( p = 0 ; p < g_weightsLen; p++ )
		{
			g_weights[p] = w[p]+getRandomdouble(-0.01,0.01);
		}

	}
	else if (operation > 7) /// 9 8 7 6
	{
		g_type = type_mutate;
		/// mutate
		DEBUG puts("Mutate");
		int parent =getParent(g_generationBest);
		double w[g_weightsLen];
		double v[Variable_Count];
		char filename[60];
		sprintf(filename,"gene_%d.txt.sort", g_generation);
		readFromFile(filename,parent,&v[0],&w[0]);

    	char bot_name[32];
    	sprintf(bot_name,"%02d%02dM_%s_",(int)(rand()%99),g_generation,g_myname );
		memset(g_myname,'\0',32);
		strcpy(g_myname,bot_name );
		printf("Join race %s\n",g_myname);
		strcpy(g_myname,bot_name );


		DEBUG printf("Mutate %d\n", parent);
		int op;

		for ( i = 0 ; i < Variable_Count; i++ )
		{
			op = rand()%10;
			if (op > 6)
			{

				g_variables[i] = v[i];
				DEBUG printf("#");

			}
			else if ( op > 2)
			{

				g_variables[i] = (v[i]+getRandomdouble(-0.2,0.2));
				DEBUG printf("=");
			}
			else
			{

				g_variables[i] = getRandomdouble(-RANGE,RANGE);
				DEBUG printf("_");
			}


		}

		//int p=0;
		int p=0;
		for ( p = 0 ; p < g_weightsLen; p++ )
		{
			op = rand()%10;
			if (op > 6)
			{

				g_weights[p] = w[p];
				DEBUG printf("#");

			}
			else if ( op > 2)
			{

				g_weights[p] = (w[p]+getRandomdouble(-0.2,0.2));
				DEBUG printf("=");
			}
			else
			{

				g_weights[p] = getRandomdouble(-RANGE,RANGE);
				DEBUG printf("_");
			}
			//g_weights[p] = getRandomdouble(-1.0,1.0);
			//rintf("%d -> %d\t: %f\n", i,j,g_weights[p]);
			printf("W %d ", p);

		}

		DEBUG printf("\nMutated\n");


	}
	else if (operation > 2 ) // 3 4 5
	{
		g_type = type_crossover;
		/// Crossover
		DEBUG puts("Crossover");
		int parent0 =getParent(g_generationBest);
		int parent1 =parent0;
		while(parent1==parent0) parent1=  getParent(g_generationBest);

		char filename[60];
		sprintf(filename,"gene_%d.txt.sort", g_generation);
		double v0[g_weightsLen];
		double v1[g_weightsLen];
		double w0[g_weightsLen];
		double w1[g_weightsLen];
		readFromFile(filename,parent0,&v0[0],&w0[0]);
		readFromFile(filename,parent1,&v1[0],&w1[0]);

    	char bot_name[32];
    	sprintf(bot_name,"%02d%02dC_%s_",(int)(rand()%99),g_generation,g_myname );
		memset(g_myname,'\0',32);
		strcpy(g_myname,bot_name );
		printf("Join race %s\n",g_myname);
		strcpy(g_myname,bot_name );

		DEBUG printf("Crossover %d - %d\n", parent0 , parent1);

		int op;
		for ( i = 0 ; i < Variable_Count; i++ )
		{
			op = rand()%2;
			if (op==0)
			{
				/// 50% chance of parent 0
				g_variables[i] = w0[i];
				DEBUG printf("_");

			}
			else
			{
				/// 25 %chance of small adjust
				g_variables[i] = w1[i];
				DEBUG printf("=");
			}
		}
		int p=0;
		for ( p = 0 ; p < g_weightsLen; p++ )
		{

			op = rand()%2;
			if (op==0)
			{
				/// 50% chance of parent 0
				g_weights[p] = w0[p];
				DEBUG printf("_");

			}
			else
			{
				/// 25 %chance of small adjust
				g_weights[p] = w1[p];
				DEBUG printf("=");
			}

			//g_weights[p] = getRandomdouble(-RANGE,RANGE);
			//printf("%d -> %d\t: %f\n", i,j,g_weights[p]);
			//p++;

		}
		DEBUG printf("\nCrossover done\n");

	}else /// 0 1
	{
		if (g_generation > 12) /// don't allow randoms anymore
		{
			int age = rand()%3+1;
			g_type = type_static;
			DEBUG puts("Ancestor");
			int parent =getParent(g_generationBest/2);
			double w[g_weightsLen];
			double v[Variable_Count];
			char filename[60];
			sprintf(filename,"gene_%d.txt.sort", g_generation-age);

	    	char bot_name[32];
			sprintf(bot_name,"%02dA%d_%s_",g_generation,age,g_myname );
			memset(g_myname,'\0',32);
			strcpy(g_myname,bot_name );
			printf("Join race %s\n",g_myname);
			strcpy(g_myname,bot_name );

			readFromFile(filename,parent,&v[0],&w[0]);

			for ( i = 0 ; i < Variable_Count; i++ )
				g_variables[i]  = v[i]+getRandomdouble(-0.05,0.05);

			int p=0;
			for ( p = 0 ; p < g_weightsLen; p++ )
			{
					g_weights[p] = w[p]+getRandomdouble(-0.05,0.05);

			}

		}
		else
		{

			char bot_name[32];
			sprintf(bot_name,"%02d%02dR_%s_",(int)(rand()%99),g_generation,g_myname );
			memset(g_myname,'\0',32);
			strcpy(g_myname,bot_name );
			printf("Join race %s\n",g_myname);
			strcpy(g_myname,bot_name );

			g_type= type_random;
			/// random
			DEBUG printf("New random\n");
			randomize();

		}
	}


	int p=0;
	for ( i = 0 ; i < Variable_Count; i++ )
		printf("V%d\t: %f\n", i,g_variables[i]);
	for ( i = 0 ; i < Input_Count; i++ )
	{
		//g_weights[p] = getRandomdouble(-RANGE,RANGE);
		printf("%d\t: %f\n", i,g_weights[p]);
		p++;
	}
	for ( i = 0 ; i < Input_Count; i++ )
	{
		for (j = i ; j< Input_Count ; j++)
		{
			//g_weights[p] = getRandomdouble(-RANGE,RANGE);
			printf("%d -> %d\t: %f\n", i,j,g_weights[p]);
			p++;
		}
	}
	for ( i = 0 ; i < Variable_Count; i++ )
	{
		g_variables[i] = getRandomdouble(-RANGE,RANGE);
	}
/*
	for ( i = 0 ; i < Input_Count; i++ )
	{
		for (j = i ; j< Input_Count ; j++)
		{
			//g_weights[p] = getRandomdouble(-1.0,1.0);
			//printf("%d -> %d\t: %f\n", i,j,g_weights[p]);
			p++;
		}
	}
*/
}

int countlines(const char* filename)
{
	FILE *fp;
	fp = fopen(filename,"r");

	int lines = 0;
	if (fp)
	{
		char ch;
		while(!feof(fp))
		{
		  ch = fgetc(fp);
		  if(ch == '\n')
		  {
			lines++;
		  }
		}
		fclose(fp);
	}
	else
	{
		puts("No file");
	}
	DEBUG printf("Check file %s : %d\n", filename,lines);

	return lines;
}
void resolveWeightsLen()
{
	/// just being lazy
	int p = 0;
	int i,j;
	for ( i = 0 ; i < Input_Count; i++ )
	{
		for (j = i ; j< Input_Count ; j++)
		{
			p++;
		}
	}
	g_weightsLen = p + Input_Count;

}

void getGenerationSize()
{

	FILE *myfile;
	myfile= fopen("GENERATION_SIZE","r");
	if (myfile != NULL)
	{
		fscanf(myfile,"%d",&g_generationSize);
		DEBUG printf("Generation size %d\n",g_generationSize);
		fclose(myfile);
	}

	myfile= fopen("GENERATION_BEST","r");
		if (myfile != NULL)
		{
			fscanf(myfile,"%d",&g_generationBest);
			DEBUG printf("Generation best %d\n",g_generationBest);
			fclose(myfile);
		}

	//g_generationBest = 20;
	//g_generationSize = 60;

}

void setGeneration()
{
	DEBUG puts("setGeneration ");

	int g = 0;

	while (1)
	{
		char filename[60];
		sprintf(filename,"gene_%d.txt", g+1);
		//string f(filename);
		int lines =  countlines(filename);
		if ( lines < g_generationSize)
		{
			break;
		}
		g++;
	}
	g_generation = g;
	DEBUG printf("Generation %d\n", g_generation);



}

void checkCurrentGen()
{
	DEBUG puts("checkGeneration ");


	char filename[60];
	sprintf(filename,"gene_%d.txt", g_generation+1);
	//string f(filename);
	int lines =  countlines(filename);
	DEBUG printf("Generation %d %d > %d\n", g_generation, lines,g_generationSize);
	if ( lines >= g_generationSize)
	{

		DEBUG printf("Generation ready!\n");
		sleep(10);
		char buf[256];
		DEBUG printf("Run shell-> \n");
		sprintf(buf, "/bin/python2 sort.py %s", filename);
		DEBUG printf("RUN : %s\n", buf);
		system(buf);

	}

}

void setInput(int input, double value)
{
	if (g_inputs && input < Input_Count)
	{
		g_inputs[input] = value;
		//printf("%d = %f\n",input,value);
	}
	else
	{
		ERRORFAIL;
	}
}
double sigmoid(double x)
{
     return 1 / (1 + exp((double) -x));
}

double getOutput()
{
	if (!g_inputs || !g_weights)
		ERRORFAIL;

	double sum = 0;
	int p = 0;
	int i,j;
	for (i = 0 ; i < Input_Count ; i++ )
	{
		sum += g_inputs[i]*g_weights[p];
		p++;
	}

	for (i = 0 ; i < Input_Count ; i++ )
	{
		for (j = i ; j < Input_Count ; j++ )
		{
			/*if (i == j)
			{
				sum += g_inputs[i]*g_weights[p];
			}
			else
			{*/
				sum += g_inputs[i]*g_inputs[j]*g_weights[p];
			//}

			p++;
		}
	}
	//// std::cout << "Sum " << sum << " sig "<<  sigmoid(sum) << std::endl;
	//printf("> %f >> %f\n", sum , sigmoid(sum));
	return sigmoid(sum);
}


/// Connection stuff
static void error(char *fmt, ...)
{
    char buf[BUFSIZ];

    va_list ap;
    va_start(ap, fmt);
    vsnprintf(buf, BUFSIZ, fmt, ap);
    va_end(ap);

    if (errno)
        perror(buf);
    else
        fprintf(stderr, "%s\n", buf);

    exit(1);
}

static int connect_to(char *hostname, char *port)
{
    int status;
    int fd;
    char portstr[32];
    struct addrinfo hint;
    struct addrinfo *info;

    memset(&hint, 0, sizeof(struct addrinfo));
    hint.ai_family = PF_INET;
    hint.ai_socktype = SOCK_STREAM;

    sprintf(portstr, "%d", atoi(port));

    status = getaddrinfo(hostname, portstr, &hint, &info);
    if (status != 0) error("failed to get address: %s", gai_strerror(status));

    fd = socket(PF_INET, SOCK_STREAM, 0);
    if (fd < 0) error("failed to create socket");

    status = connect(fd, info->ai_addr, info->ai_addrlen);
    if (status < 0) error("failed to connect to server");

    freeaddrinfo(info);
    return fd;
}

static void log_message(char *msg_type_name, cJSON *msg)
{
    cJSON *msg_data;
    //printf("data %s\n",msg_type_name);
    if (!strcmp("join", msg_type_name)) {
        puts("Joined");
    } else if (!strcmp("lapFinished", msg_type_name)) {
        puts("lapFinished");
        cJSON* data = cJSON_GetObjectItem(msg, "data");
        {
        	cJSON* car = cJSON_GetObjectItem(data,"car");
        	if ( car)
        	{
        		cJSON* name = cJSON_GetObjectItem(car,"name");

        		if (name && strcmp(name->valuestring, g_myname)==0)
        		{
        			if(g_current_tick > 200)
        			{
        				g_laps++;
        			}

        			DEBUG puts("My lap");
        			cJSON* lap = cJSON_GetObjectItem(data,"lapTime");
        			if (lap)
        			{
        				cJSON* millis = cJSON_GetObjectItem(lap,"millis");
        				if (millis)
        				{
							double t = millis->valuedouble;
							DEBUG printf("LAP TIME %lf\n", t);
							if (t > g_bestLap)
							{
								g_bestLap = t;
								g_score = t;
								DEBUG printf("Best lap!\n");
							}
        				}
        			}

        		}
        	}
        }
    }else if (!strcmp("gameStart", msg_type_name)) {
        puts("Race started");
    } else if (!strcmp("crash", msg_type_name)) {


    	DEBUG puts("crash");
        cJSON* data = cJSON_GetObjectItem(msg, "data");
        if (data)
        {
        	cJSON* name = cJSON_GetObjectItem(data,"name");
        	if ( name)
        	{
        		if ( strcmp(name->valuestring, g_myname)==0)
        		{
        			g_crash++;
        			DEBUG puts("I crashed :(");
        			g_inTrack= 0;
        			if ( g_track_knownfast[g_piece] > g_inputs[Input_M3_Velocity])
        			{
        				g_track_knownfast[g_piece] = g_inputs[Input_M3_Velocity]*0.8;

        			}

        			if (g_track_knownfast[g_piece] < 1)
        				g_track_knownfast[g_piece] = 1;

        		}
        		else
        		{
        			int o;
        			for(o = 0 ; o < g_allCarsInfoN ; o++)
        			{
        				if (  strcmp(name->valuestring,g_carNames[o]) == 0)
        				{
        					printf("%s crashed\n",g_carNames[o]);
        					//g_track_speedlimit[carInfo_active] = 0.00;
        					if ( g_allCarsInfo[o][carInfo_angle] > 45)
							{


								printf(" Set speedlimit\n");
								if(g_allCarsInfo[o][carInfo_velocity] > 0 && g_allCarsInfo[o][carInfo_velocity] < 12)
								{
									g_allCarsInfo[o][carInfo_crash]+=1;
									int prev = g_allCarsInfo[o][carInfo_pieceN] -1;
									int prev2 = g_allCarsInfo[o][carInfo_pieceN] -2;
									if ( prev < 0 )
										prev = g_trackLen -1 ;
									if ( prev2 < 0 )
										prev2 = g_trackLen +prev2 ;
									if ( g_allCarsInfo[o][carInfo_velocity]*0.90 < g_track_speedlimit[(int)g_allCarsInfo[o][carInfo_pieceN]] )
										g_track_speedlimit[(int)g_allCarsInfo[o][carInfo_pieceN]] = g_allCarsInfo[o][carInfo_velocity]*0.90;
									if ( g_allCarsInfo[o][carInfo_velocity]*0.93 < g_track_speedlimit[prev] )
										g_track_speedlimit[prev] = g_allCarsInfo[o][carInfo_velocity]*0.93;
									if ( g_allCarsInfo[o][carInfo_velocity]*0.95 < g_track_speedlimit[prev2] )
											g_track_speedlimit[prev2] = g_allCarsInfo[o][carInfo_velocity]*0.95;
									printf(" Set speedlimit %d : %lf\n",prev2,g_track_speedlimit[prev2]);
									printf(" Set speedlimit %d : %lf\n",prev,g_track_speedlimit[carInfo_pieceN]);
									printf(" Set speedlimit %d : %lf\n",(int)g_allCarsInfo[o][carInfo_pieceN],g_track_speedlimit[prev]);
								}

							}
        				}
        			}
        		}

        	}
        }

    } else if (!strcmp("gameEnd", msg_type_name)) {

        g_trackInitialized = 0;
        g_inTrack = 0;
        DEBUG puts("Race ended");
        cJSON* data = cJSON_GetObjectItem(msg, "data");
        if (data)
        {
        	cJSON* bestlap = cJSON_GetObjectItem(data, "bestLaps");
        	if (bestlap)
        	{
        		int carsN =  cJSON_GetArraySize(bestlap);
        		int i;
        		for (i = 0 ; i < carsN ; i++)
        		{
        			cJSON * car = cJSON_GetArrayItem(bestlap, i);


        			cJSON * carid = cJSON_GetObjectItem(car, "car");
					if (carid)
					{

						cJSON * carname = cJSON_GetObjectItem(carid, "name");

						if (carname)
						{
							DEBUG printf("carName : %s\n", carname->valuestring);
							if ( strcmp(carname->valuestring, g_myname)==0)
							{
								DEBUG printf("That's me\n");
								cJSON * result = cJSON_GetObjectItem(car, "result");
								if (result)
								{
									cJSON * millis = cJSON_GetObjectItem(result, "millis");
									if (millis)
									{
										double m = millis->valuedouble;
										DEBUG printf("Millis %lf\n", m);
										g_score = m;
										if (g_score < 0 && g_score > g_bestLap)
										{
											DEBUG puts("Negative time!");
										}
										g_hasScore = 1;

									}
								}
							}
						}

					}
        		}
        	}
        }
        clear_trackInfo();
        /*
         *
        "bestLaps": [
            {
              "car": {
                "name": "Schumacher",
                "color": "red"
              },
              "result": {
                "lap": 2,
                "ticks": 3333,
                "millis": 20000
              }
            },
            {
              "car": {
                "name": "Rosberg",
                "color": "blue"
              },
              "result": {}
            }
          ]
          */


    } else if (!strcmp("gameInit", msg_type_name)) {
    	if (g_trackInitialized==0)
    	{
			DEBUG puts("Race init");

					cJSON* data = cJSON_GetObjectItem(msg, "data");
					if ( data )
					{
						DEBUG puts("Data");
						cJSON* race = cJSON_GetObjectItem(data, "race");
						if (race)
						{
							DEBUG puts("Race");
							cJSON* track  = cJSON_GetObjectItem(race, "track");
							if (track )
							{
								DEBUG puts("track");
								cJSON * pieces = cJSON_GetObjectItem(track,"pieces");
								if (pieces)
								{
									DEBUG puts("pieces");
									int i;
									g_trackLen =  cJSON_GetArraySize(pieces);
									DEBUG printf("%d track pieces\n", g_trackLen);
									g_track = (double**)malloc(sizeof(double*) * g_trackLen);
									if(!g_track)
									{
										ERRORFAIL;
									}

									g_tracklencum = (double*)malloc(sizeof(double) * g_trackLen );
									if(!g_tracklencum)
									{
										ERRORFAIL;
									}

									g_track_knownfast = (double*)malloc(sizeof(double) * g_trackLen );
									if(!g_track_knownfast)
									{
										ERRORFAIL;
									}

									g_track_speedlimit = (double*)malloc(sizeof(double) * g_trackLen );
									if(!g_track_speedlimit)
									{
										ERRORFAIL;
									}
									double l = 0;
									for (i = 0 ; i < g_trackLen ; i++)
									{
										g_track[i] = (double*) malloc(sizeof(double)*PIECE_COUNT);

										if (g_track[i]==NULL)ERRORFAIL;

										cJSON * piece = cJSON_GetArrayItem(pieces, i);
										if (piece)
										{
											cJSON * length = cJSON_GetObjectItem(piece, "length");
											cJSON * angle = cJSON_GetObjectItem(piece, "angle");
											cJSON * radius = cJSON_GetObjectItem(piece, "radius");
											cJSON * sw = cJSON_GetObjectItem(piece, "switch");

											g_track[i][PIECE_CARS]=0;
											g_track_speedlimit[i] = 20;
											if ( length != NULL)
											{
												g_track[i][PIECE_LENGTH] = length->valuedouble;
											}
											else
											{
												g_track[i][PIECE_LENGTH] = 0.0;
											}

											if ( angle != NULL)
											{
												g_track[i][PIECE_ANGLE] = angle->valuedouble;
											}
											else
											{
												g_track[i][PIECE_ANGLE] = 0.0;
											}

											if ( radius != NULL)
											{
												g_track[i][PIECE_RADIUS] = radius->valuedouble;
												g_track_speedlimit[i] = (10.0-(S(radius->valuedouble)*(fabs(g_variables[Variable_Tactics]))))*1.3;
												//if (g_track_speedlimit[i]<4.0)
												//	g_track_speedlimit[i] = 4.0;
												if ( i > 0 && g_track_speedlimit[i-1] > 10)
												{
													g_track_speedlimit[i-1] = (10.0-(S(radius->valuedouble)*(fabs(g_variables[Variable_Tactics]))));

												}

												if ( i > 1 && g_track_speedlimit[i-2] > 10)
												{
													g_track_speedlimit[i-1] = (10.0-(S(radius->valuedouble)*(fabs(g_variables[Variable_Tactics]))))*1.3;

												}
											}
											else
											{
												g_track[i][PIECE_RADIUS] = 0.0;
											}

											if ( sw != NULL && sw->type == cJSON_True)
											{
												g_track[i][PIECE_SWITCH] = 1.0;
											}
											else
											{
												g_track[i][PIECE_SWITCH] = 0.0;

											}
	#define PI 3.14159265359
											if ( fabs(g_track[i][PIECE_ANGLE]) > 0 )
											{
												g_track[i][PIECE_LENGTH] = ( fabs(g_track[i][PIECE_ANGLE])*PI*g_track[i][PIECE_RADIUS])/180.0 ;
											}

											g_tracklencum[i] = l;
											l += g_track[i][PIECE_LENGTH];
											g_track_knownfast[i] = -100;


											DEBUG printf("#%d\tLen%f A%f R%f S%d\tSUM %lf\n",
													i, g_track[i][PIECE_LENGTH],g_track[i][PIECE_ANGLE],
													g_track[i][PIECE_RADIUS],(int)g_track[i][PIECE_SWITCH], g_tracklencum[i]);



										}
									}
								}
							} // Pieces
							cJSON * lanes = cJSON_GetObjectItem(track,"lanes");
							if (lanes)
							{
								DEBUG puts("Lanes");
								g_lanesLen =  cJSON_GetArraySize(lanes);
								g_lanes = (double*)malloc(g_lanesLen*sizeof(double));
								if (!g_lanes)ERRORFAIL;
								int i;
								for (i = 0 ; i < g_lanesLen ; i++)
								{
									cJSON * lane = cJSON_GetArrayItem(lanes, i);
									cJSON * dist = cJSON_GetObjectItem(lane, "distanceFromCenter");
									if (dist)
									{
										g_lanes[i] = dist->valuedouble;
									}
									else
									{
										g_lanes[i] = 0;
										puts("No lane distanceFromCenter");
									}
									DEBUG printf("#%d\tDist%f\n",i,g_lanes[i]);
								}




							} /// Lanes


						} /// race
						cJSON * cars = cJSON_GetObjectItem(race,"cars");
						if (cars)
						{
							clearCarInfo();
							initCarInfo(cJSON_GetArraySize(cars));
							printf("Cars #%d\n",cJSON_GetArraySize(cars));
							int i;
							for (i = 0 ; i < cJSON_GetArraySize(cars) ; i++)
							{
								cJSON * car = cJSON_GetArrayItem(cars, i);
								if (car)
								{
									cJSON * carid = cJSON_GetObjectItem(car, "id");
									if (carid)
									{

										cJSON * carname = cJSON_GetObjectItem(carid, "name");
										cJSON * carcolor = cJSON_GetObjectItem(carid, "color");
										if (carname && carcolor)
										{
											printf("Car #%d\t%s (%s) \n", i,carname->valuestring,carcolor->valuestring);
											if (carname && strcmp(carname->valuestring,g_myname)==0)
											{
												DEBUG puts("I'm racing :)");
											}
											strcat( g_carNames[i] ,  carname->valuestring);

											// g_allCarsInfo[i];

											cJSON * cardim = cJSON_GetObjectItem(car, "dimensions");
											if (cardim)
											{
												cJSON * carlen = cJSON_GetObjectItem(cardim, "length");
												cJSON * carwidth = cJSON_GetObjectItem(cardim, "width");
												cJSON * carflag = cJSON_GetObjectItem(cardim, "guideFlagPosition");

												if (carlen)
												{
													g_carLength = carlen->valuedouble;
												}
												else
												{
													DEBUG puts("No car length");
												}

												if (carwidth)
												{
													g_carWidth = carwidth->valuedouble;
												}
												else
												{
													DEBUG puts("No car width");
												}

												if (carflag)
												{
													g_carFlagpos = carflag->valuedouble;
												}
												else
												{
													DEBUG puts("No car flag position");
												}

												DEBUG printf("Car L%f x W%f  (%f)\n",g_carLength,g_carWidth,g_carFlagpos);

											}
										}


									}



								}
								else
								{
									puts("Cannot read car");
								}
							}
							/// Check that I am on the race

						} /// cars
						else
						{
							puts("No cars");
						}

						cJSON * info = cJSON_GetObjectItem(race,"raceSession");
						if (info)
						{
							DEBUG printf("raceSession\n");
							cJSON * laps = cJSON_GetObjectItem(info,"laps");
							cJSON * quickRace = cJSON_GetObjectItem(info,"quickRace");
							if (laps)
							{
								g_totalLaps = laps->valueint;

							}
							else
							{
								DEBUG puts("No number of laps?");
								DEBUG puts("Assume 5. 5 is nice number of traks. This may be a qualification round...");
								g_totalLaps=5;

								///ERRORFAIL;

							}

							if (quickRace)
							{
								if (quickRace->type == cJSON_True)
								{
									printf("Quickrace\n");
								}
								else if (quickRace->type == cJSON_True)
								{
									printf("Not quickrace\n");
									printf("This might be an actual competition :)\n");
								}
								else
								{
									printf("No quickrace info...\n");
								}
							}
							else
							{
								puts("No quickrace info...");
							}


							g_trackTotalLength = g_totalLaps * (g_tracklencum[ g_trackLen-1] + g_track[ g_trackLen-1][PIECE_LENGTH] );


							/// Check that I am on the race

						} /// cars
						else
						{
							DEBUG puts("No cars");
						}


					}/// data
					else
					{
						DEBUG puts("No data!");
					}

					DEBUG puts("Race init done");
					g_trackInitialized = 1;
					g_inTrack = 1;
    	}/// If not initializsed yet, don't clear knownfasts and speedlimits after qualification
    	else
    	{
    		DEBUG puts("Track already initialized");
    	}

    }
    else if (!strcmp("spawn", msg_type_name))
    {
    	DEBUG puts("spawn");
    	cJSON* data = cJSON_GetObjectItem(msg, "data");
		if (data)
		{
			cJSON* name = cJSON_GetObjectItem(data,"name");
			if ( name)
			{
				if ( strcmp(name->valuestring, g_myname)==0)
				{

					DEBUG puts("I spawned :)");
					g_inTrack= 1;
				}
			}
		}

    } else if (!strcmp("finish", msg_type_name)) {
    	DEBUG puts("finish");
    } else if (!strcmp("dnf", msg_type_name)) {
    	DEBUG puts("Did not finish");
    } else if (!strcmp("turboAvailable", msg_type_name)) {
    	DEBUG puts("Turbo");
        g_turboavailable = 1;
        g_turbotick = 30;
        cJSON* tick = cJSON_GetObjectItem(msg, "turboDurationTicks");
        if (tick )
        {
        	g_turbotick = tick->valueint;
        }
        else
        {
        	puts("No turbotick!");
        }
    }else if (!strcmp("tournamentEnd", msg_type_name)) {
        DEBUG puts("/////// TOURNAMENT END /////////");
        g_ready = 1;

    }else if (!strcmp("error", msg_type_name)) {
        msg_data = cJSON_GetObjectItem(msg, "data");
        if (msg_data == NULL)
            puts("Unknown error");
        else
        {
            printf("ERROR: %s\n", msg_data->valuestring);
            if ( strstr(msg_data->valuestring,"60 se") )
            {
            	int wait;
				for ( wait = 0 ;wait < 20 ; wait++)
					{printf(". ",wait);sleep(2);}
            }
        }
    }
}
#define CLOCKTYPE CLOCK_MONOTONIC
int main(int argc, char *argv[])
{
	printf("Antikamel race bot ver 2\n");
    int sock;
    cJSON *json;
    g_mode = 0;
    int botNum = 0;
    int raceNum = 0;
    if (argc == 5 || argc == 7|| argc == 8)
    {
    	/// Not learning
    	g_mode = 0;
    	g_verbose = 0;
    	puts("Run bot");
    	if ( argc > 7 )
    	{
        	if (atoi(argv[5])==1)
        	{
        		g_verbose = 1;
        		puts("Verbose");
        	}

        	else
        	{
        		g_verbose = 0;
        		puts("quiet");
        	}

        	botNum = atoi(argv[6]);
        	raceNum = atoi(argv[7]);
        	printf("Bot %d\n",botNum);
        	printf("Race %d\n",raceNum);
        	g_mode =2;
    	}

    }
    else if (argc == 6)
    {
    	/// Learning
    	if (atoi(argv[5])==1)
    		g_verbose = 1;
    	else
    		g_verbose = 0;
    	g_mode = 1;
    	puts("Practice");

    }
    else
    {
        error("Usage: bot host port botname botkey\n");
    }



    /***
     *  Prepare stuff
     */
    g_goFaster = 0;
    g_bestLap = 0;
    g_maxSpeed = 0;
    		g_slowdown=-1;
    		g_antiskip=-1;
    g_minAngle = 90;
    g_maxAngle = 0;
    g_allCarsInfo = 0;
    g_type = 0;
    g_tracklencum = 0;
    g_trackInitialized = 0;
    g_trackLen = 0;
    g_track = NULL;
    g_inTrack = 0;
    g_lanes = NULL;
    g_throttle = 0;
    g_carLength = 40;
    g_carWidth = 20;
    g_carFlagpos = 0;
    g_abort = 0;
    g_crash = 0;
    g_pos = 0;
    g_weightsLen = 0;
    g_piece = 0;
    g_startTick = -1;
    g_laps = 0;
    g_changing_lane = 0;
    g_score=0.0;
    g_ready = 0;
    g_turboavailable = 0;
    g_totalLaps = 3;
    g_hasScore=0;
    g_switching = 0 ;
    g_track_knownfast = 0;
    g_trackTotalLength = 0;
    int maxCrash = 0;
    g_last_current_tick = 1;
    g_lastAngle = 0;
    //// Initialize GA
    getWriteLock();
    srand((time(NULL) & 0xFFFF) | (getpid() << 16));
    srandom((time(NULL) & 0xFFFF) | (getpid() << 16));
   // srand(time(NULL));
   // srandom(time(NULL));

    memset(g_myname,'\0',32);
    strcpy(g_myname,argv[3] );

    if ( g_mode == 1)
    {
    	puts("Mode1");
    	/// Learn
		resolveWeightsLen();
		getGenerationSize();
		setGeneration();
		initializeGA();
		setWeights();
		botNum = 0;
		//setupBot(botNum);

    }
    else
    {

    	/// Race
		puts("Mode0");
		getGenerationSize();
		//setGeneration();
		resolveWeightsLen();
		initializeGA();
		botNum = 0;
		setupBot(botNum);
/*
		char name[16];
		sprintf(name , "%s_%d",argv[3] , botNum );
		memset(g_myname,'\0',32);
		strcpy(g_myname, name);
		*/

    }
    freeWriteLock();

    /**
     *
     */


    sock = connect_to(argv[1], argv[2]);

    if (g_mode == 1 || g_mode==2)///Learning
    {
    	puts("Run test");
		g_verbose = 1;
		//json = joinduel_msg(argv[3], argv[4]);
		//int r = rand()%3;
		//if (r==0)
		///////////////////////////////json = join_msg(argv[3], argv[4]);

		char bot_name[16];
	//	sprintf(bot_name,"_%d_%s",(int)(rand()%99),g_myname );
	//	memset(g_myname,'\0',32);
	//	strcpy(g_myname,bot_name );
		//json = join_france_msg(g_myname, argv[4],3);
		g_verbose = 1;


    	maxCrash = 1;
    	int race = 1;
    	// if ( g_mode==1)
    		 race = (int)(g_generation/5.0)%6;
    	// else
    	//	 race = raceNum;

    	int cars = 1;
    	//if (g_generation > 12 )
    	//	cars = 4;
    	//race = 1; /// Japan is good to test
    	//race = 3-(g_generation%4);
    	//if ( g_generation  % 5 == 0  || (g_generation)  % 5 == 1)
    	//	cars = 3; /// So wont stuck waiting for other cars when they change track (so easily)
    	if ( g_generation   == 0 )
    		cars = 3;
    	printf("G%d Race %d Cars %d\n", g_generation,race,cars);
    	if (race == 3)
    	{
        	puts("Join germany");
        	json = join_race_msg("germany",g_myname, argv[4],cars);

    	}
    	if ( race == 0)
    	{
    		puts("Join france");
    		json = join_race_msg("france",g_myname, argv[4],cars);

    	}
    	if (race == 5)
    	{
    		//puts("Join usa");
    		//json = join_usa_msg(g_myname, argv[4],cars);
    		puts("Join elaintarha");
    		json = join_race_msg("elaeintarha", g_myname, argv[4],cars);
    	}
    	if (race == 4)
    	{
    		puts("Join japan");
    		json = join_race_msg("suzuka" , g_myname, argv[4],cars);
    	}
    	if (race == 2)
    	{
    		puts("Join Italy");
    		json = join_race_msg("imola" , g_myname, argv[4],cars);
    	}
    	if (race == 1)
    	{
    		puts("Join england");
    		json = join_race_msg("england" , g_myname, argv[4],cars);
    	}
    	//cJSON_AddStringToObject(json, "password", "kamelirace");
    	write_msg(sock, json);

    }
    else
    {
    	maxCrash = 100;

		puts("Run final");
		g_verbose = 0;
		//json = joinduel_msg(argv[3], argv[4]);
		//int r = rand()%3;
		//if (r==0)
		json = join_msg(argv[3], argv[4]);
		/*
		char bot_name[16];
		sprintf(bot_name,"_%d_%s",(int)(rand()%99999),g_myname );
		memset(g_myname,'\0',32);
		strcpy(g_myname,bot_name );
		json = join_germany_msg(g_myname, argv[4],4);
		g_verbose = 1;
		cJSON_AddStringToObject(json, "password", "kamelirace");
		*/
		//else if (r==1)
		//	json = join_germany_msg(argv[3], argv[4]);
		//else
		//	json = join_usa_msg(argv[3], argv[4]);
		write_msg(sock, json);

    }

    //cJSON_Delete(json);
    printf("Start %s\n", g_myname);
    //cJSON_Delete(json);
    json = 0;
    while ((json = read_msg(sock)) != NULL)
    {
	  struct timespec tsi, tsf;

	  clock_gettime(CLOCKTYPE, &tsi);

	  DEBUG printf("\n");


    	if (g_mode == 1)
    	{
    		/// First generation has different rules here

			if ((g_crash>0&&g_laps==0)||(g_laps>0&&g_crash>maxCrash))
			{
				DEBUG puts("Cannot stay on the track");
				static int crashAbortTimer = 20;
				crashAbortTimer--;
				if ( crashAbortTimer < 1)
					g_abort = 1;
				break;

			}

			if (g_abort==1)
			{
				DEBUG puts("Someone decided that this one is not fit. Breaking loop");
				break;
			}

			if (g_ready ==1 && g_hasScore > 0)
			{
				puts("Done");
				break;
			}

			if (g_laps > 2)
			{
				g_hasScore = 1;
				g_score = g_bestLap;
				g_ready = 1;
			}


    	}

    	int sent = 0;
        cJSON *msg, *msg_type;
        msg = 0;

        char *msg_type_name;
        g_current_tick = -1;
        cJSON *tickmsg = cJSON_GetObjectItem(json, "gameTick");
        msg_type = cJSON_GetObjectItem(json, "msgType");
        if (msg_type == NULL)
            error("missing msgType field");
        if (tickmsg)
        {
        	g_current_tick = tickmsg->valueint;
        	//DEBUG printf("%s\n",msg_type->valuestring);
        	//DEBUG printf("G%d Tick %d -> %d : %d\t\n",g_generation,g_startTick ,g_current_tick, g_current_tick-g_startTick);
        	DEBUG printf("G%d",g_generation);
        	if (g_verbose)
        	{
				if (g_type == type_random) printf("R");
				else if (g_type == type_static) printf("S");
				else if (g_type == type_mutate) printf("M");
				else if (g_type == type_crossover) printf("C");
				else printf("?");
        	}

        	DEBUG printf(" %3.1lf LAP %d/%d BEST %5.1lf\n", g_myTrackPos/(g_trackTotalLength)*100.0,g_laps ,g_totalLaps,g_bestLap>0?g_bestLap:0);
        }





        msg_type_name = msg_type->valuestring;
        if (!strcmp("carPositions", msg_type_name))
        {
        	int o;
			for (o = 0 ; o < g_trackLen ; o++)
				g_track[o][PIECE_CARS]=0;
        	int mydata = -1;
        	if (!g_trackInitialized)
        	{
        		printf("Game is running but track has not been initialized! :(\n");
        	}

        	if (!g_inTrack)
        	{
        		puts("out");
        		msg = ping_msg();
        	}
        	else
        	{
				//g_score+=0.1;
				//puts("CarPosition data");
				if (g_startTick==-1)
					g_startTick = g_current_tick;
				/// Find my car
				cJSON * data = cJSON_GetObjectItem(json, "data");
				if (data)
				{

					//printf("data %d\n", cJSON_GetArraySize(data));
					int i;
					cJSON * myCar = 0;
					for (i = 0; i < cJSON_GetArraySize(data) ; i++)
					{

						//puts("car");
						cJSON * carItem = cJSON_GetArrayItem(data, i);

						if (carItem)
						{
							cJSON * id = cJSON_GetObjectItem(carItem, "id");
							if (id)
							{
								cJSON * name = cJSON_GetObjectItem(id, "name");
								//printf("%s %s\n", name->valuestring, g_myname);
								if (name && strcmp(name->valuestring,g_myname)==0)
								{
									//puts("My data");
									mydata=i;

								}
								else
								{
									/// Someone else
									//printf("Conpetitioner info %d\n",i);
									/// Save info
									if (g_allCarsInfo)
									{

										cJSON * angle 				= cJSON_GetObjectItem(carItem, "angle");
										cJSON * piecePosition 		= carItem?cJSON_GetObjectItem(carItem, "piecePosition"):NULL;
										cJSON * pieceIndex 			= piecePosition?cJSON_GetObjectItem(piecePosition, "pieceIndex"):NULL;
										cJSON * inPieceDistance 	= piecePosition?cJSON_GetObjectItem(piecePosition, "inPieceDistance"):NULL;
										cJSON * lane 				= piecePosition?cJSON_GetObjectItem(piecePosition, "lane"):NULL;
										cJSON * lanestartLaneIndex 	= lane?cJSON_GetObjectItem(lane, "startLaneIndex"):NULL;
										if (angle && piecePosition && pieceIndex && inPieceDistance && lane && lanestartLaneIndex)
										{
											//g_allCarsInfo[i][carInfo_pieceN]  = 0;
											double ticks = g_current_tick - g_last_current_tick;
											g_allCarsInfo[i][carInfo_angle] = angle->valuedouble;
											g_allCarsInfo[i][carInfo_pieceN] = pieceIndex->valueint;
											g_allCarsInfo[i][carInfo_pieceD] = inPieceDistance->valuedouble;
											g_allCarsInfo[i][carInfo_crashed] = 0;

											g_allCarsInfo[i][carInfo_totalDistance] = g_tracklencum[pieceIndex->valueint] + inPieceDistance->valueint;
											//printf(" D %lf -> %lf" , g_allCarsInfo[i][carInfo_totalDistancePrev] , g_allCarsInfo[i][carInfo_totalDistance] );
											if (g_allCarsInfo[i][carInfo_totalDistance]  == g_allCarsInfo[i][carInfo_totalDistancePrev])
											{
												/*
												if (g_allCarsInfo[i][carInfo_active] > 0.00 && g_allCarsInfo[i][carInfo_angle] > 55)
												{

													printf(" Car %d crashed? ", i );
													printf(" Set speedlimit\n");
													int prev = carInfo_pieceN -1;
													if ( prev < 0 )
														prev = g_trackLen -1 ;
													g_track_speedlimit[carInfo_pieceN] = g_allCarsInfo[i][carInfo_velocity]*0.95;
													g_track_speedlimit[prev] = g_allCarsInfo[i][carInfo_velocity]*0.92;
													printf(" Set speedlimit %d : %lf\n",prev,g_track_speedlimit[carInfo_pieceN]);
													printf(" Set speedlimit %d : %lf\n",carInfo_pieceN,g_track_speedlimit[prev]);

												}
												*/
												g_allCarsInfo[i][carInfo_active] = 0.00;
											//	printf(" Out ?" );
											}
											else
											{
												g_allCarsInfo[i][carInfo_active] = 1;

											}
											if (  g_allCarsInfo[i][carInfo_totalDistance] > g_allCarsInfo[i][carInfo_totalDistancePrev])
												g_allCarsInfo[i][carInfo_velocity] = g_allCarsInfo[i][carInfo_totalDistance] - g_allCarsInfo[i][carInfo_totalDistancePrev]/(float)ticks;
											else if (  g_allCarsInfo[i][carInfo_totalDistance] < g_allCarsInfo[i][carInfo_totalDistancePrev])
											{
												g_allCarsInfo[i][carInfo_velocity] = 0.5;
											}
											if (g_allCarsInfo[i][carInfo_velocity]  > 20)
												g_allCarsInfo[i][carInfo_velocity]  = 0.6;
											//printf(" V %lf ", g_allCarsInfo[i][carInfo_velocity]);
											g_allCarsInfo[i][carInfo_totalDistancePrev] = g_allCarsInfo[i][carInfo_totalDistance];
											g_allCarsInfo[i][carInfo_lane] = lanestartLaneIndex->valueint;
											//g_allCarsInfo[i][carInfo_pieceN];

											if (pieceIndex)
												g_track[pieceIndex->valueint][PIECE_CARS] = g_track[pieceIndex->valueint][PIECE_CARS]+1.0;
											//printf("\n");
										}
										else
										{
											printf(" missing data\n");
										}
										//if (g_allCarsInfo[i][carInfo_active] > 0.0)
										//	printf("Car %d at P %d L %d A %2.1lf V%1.2lf\n",i, (int)g_allCarsInfo[i][carInfo_pieceN], (int)g_allCarsInfo[i][carInfo_lane] ,g_allCarsInfo[i][carInfo_angle] ,g_allCarsInfo[i][carInfo_velocity]);

									}


									//DEBUG puts("Not my info");

								}

							}
						}
					}
					//puts("check my data");
					myCar = cJSON_GetArrayItem(data, mydata);
					if ( mydata != -1 && myCar )
					{
						//printf("My data %d\n",mydata);
						cJSON * angle 				= cJSON_GetObjectItem(myCar, "angle");
						cJSON * piecePosition 		= angle?cJSON_GetObjectItem(myCar, "piecePosition"):NULL;
						cJSON * pieceIndex 			= piecePosition?cJSON_GetObjectItem(piecePosition, "pieceIndex"):NULL;
						cJSON * inPieceDistance 	= piecePosition?cJSON_GetObjectItem(piecePosition, "inPieceDistance"):NULL;
						cJSON * lane 				= piecePosition?cJSON_GetObjectItem(piecePosition, "lane"):NULL;
						cJSON * lanestartLaneIndex 	= lane?cJSON_GetObjectItem(lane, "startLaneIndex"):NULL;

						double 	currangle 		= 		angle?angle->valuedouble:0.0;
						int 	currlane  		= 		lanestartLaneIndex?lanestartLaneIndex->valueint:0.0;
						double 	pieceDist 	= 		inPieceDistance?inPieceDistance->valuedouble:0.0;
						int   	pieceN 		= 		pieceIndex?pieceIndex->valueint:0.0;
						int		pieceNext   =   	pieceN<(g_trackLen-1)?pieceN+1:0;

						if (fabs(currangle) > g_maxAngle) g_maxAngle = fabs(currangle);
						//if (fabs(currangle) < g_minAngle) g_minAngle = fabs(currangle);

						double myPos = (g_laps * g_trackTotalLength) +g_tracklencum[pieceN] + pieceDist ;
						double myTrackPos =g_tracklencum[pieceN] + pieceDist;
						if ( !angle || !piecePosition || !pieceIndex || !inPieceDistance || !lane || !lanestartLaneIndex )
							ERRORFAIL;




						if (pieceN < g_piece)
						{
							/// we may have started before poleposition, so skip too early laps
							if (g_current_tick > 200)
							{
								DEBUG puts(" --| LAP |--\n");

								/*
								if (g_laps == 3)
								{
									g_endTick = g_current_tick;
									//g_score = (double)counter;
									//DEBUG printf("####################\n#################\nFinished! %f\n", g_score);
									//g_ready = 1;

								}
								*/
							}
						}

						double velocity;
						double ticks = g_current_tick - g_last_current_tick;


						//printf(" My pos %lf Last pos %lf Velocity %lf\n", myPos, g_myTrackPos, velocity);
						if (ticks > 0 )
							velocity = (myPos - g_myTrackPos) / ticks;
						else
						{
							velocity = (myPos - g_myTrackPos);
							printf("Bad tick! %d %d\n",g_current_tick,g_last_current_tick );
							sent = 1;
							//continue;
						}
						g_myTrackPos = myPos;

						if (pieceN != g_piece)
						{
							///
							if (g_crash == 0)
							{
								/// record fastest valid speed in piece

								if ( g_vel > g_track_knownfast[g_piece] && g_vel > 0.2)
								{
									if ( fabs(g_track[pieceN][PIECE_ANGLE]) >0.0 )
										g_track_knownfast[g_piece] = g_vel * 0.95; /// angles are bad
									else
										g_track_knownfast[g_piece] = g_vel;
								}

							}
						}


						if (pieceN != g_piece && g_track[pieceN][PIECE_SWITCH] > 0.0)
						{
							g_changing_lane = 0;
							g_switching=0;

						}
						g_lane = currlane;
						if ( velocity < 0)
							velocity = g_vel;


						g_vel = velocity;
						g_last_current_tick = g_current_tick;
						g_pos = pieceDist;
						g_piece = pieceN;

						if (velocity > g_maxSpeed)
						{
							g_maxSpeed = velocity;
						}


						/// Find next turn ///
						double nextTurnDist=0;
						double nextTurnAngle=0;
						double nextTurnRadius=0;
						double turnLength=0;
						int k = pieceN;
						int t=0;
					//	int set =0;
						int turnbeforefinish = 1;
						/// Find nextTurnDist

						while(1)
						{

							if ( fabs(g_track[k][PIECE_ANGLE]) > 0.0 )
							{
								nextTurnAngle=fabs(g_track[k][PIECE_ANGLE]);
								nextTurnRadius = fabs(g_track[k][PIECE_RADIUS]);
								if ( g_track[k][PIECE_SWITCH] > 0.0 && g_switching ) /// Also switching in the corner
								{
									nextTurnRadius*=0.75;/// More steeper
								}
								//printf(" -->| %d is ANGLE " , k  );
								break;
							}
							else if ( k == pieceN+1 && velocity > 9 && g_track[k][PIECE_SWITCH] > 0.0 && g_switching) /// Also regard line changing as turn
							{
								nextTurnAngle=45;
								nextTurnRadius = fabs(g_track[k][PIECE_LENGTH]/1.5);
								//printf(" -->| %d is SWITCH " , k  );
								break;
							}
							else
							{
								if (k == pieceN)
								{
									nextTurnDist += (g_track[k][PIECE_LENGTH]-pieceDist);
								}
								else
								{
									nextTurnDist += g_track[k][PIECE_LENGTH];
								}
								//printf(" -->| %d %lf " , k , nextTurnDist );
							}
							k++;
							k%=g_trackLen;

							if ( g_laps == g_totalLaps-1 )
							{
								/// Final lap
								if ( k == 0)
								{
									/// 0 after increment == no turn before race end
									nextTurnDist = 1000;
									turnbeforefinish = 0;
									DEBUG printf("FINAL ");
									break;
								}
							}

							t++;
							if (t >= g_trackLen)
							{
								/// No turns?
								DEBUG printf("no turns?");
								break;
							}
						}



						/// Find turn len
						k = pieceN;
						t=0;
					//	set =0;
						/// Find next turn start
						if (nextTurnDist > velocity*40.0)
						{
							turnLength = 0;
						}
						else
						{
							while(1)
							{
								if ( fabs(g_track[k][PIECE_ANGLE]) > 0.0 )
								{
									break;
								}

								k++;
								k%=g_trackLen;

								t++;
								if (t >= g_trackLen)
								{
									puts("no turns in track?");
									/// No turns?
									break;
								}
							}

							/// Now k is start of turn
							while(1)
							{

								if ( fabs(g_track[k][PIECE_ANGLE]) > 0.0 )
								{
									if (k == pieceN)
									{
										turnLength+=(g_track[k][PIECE_LENGTH]-pieceDist);


									}
									else
									{
										turnLength+=g_track[k][PIECE_LENGTH];

									}
									nextTurnAngle+=g_track[k][PIECE_ANGLE];

								}
								else
								{
									break;
								}

								k++;
								k%=g_trackLen;

								t++;
								if (t >= g_trackLen)
								{
									///puts("Only turns in the track?");
									/// No turns?
									break;
								}
							}
						}

						if ( nextTurnAngle > 0) /// To right
						{
							nextTurnRadius -= g_lanes[currlane]; /// positive is to right ==-> positive offset
						/// is smaller radius
						}
						else
						{
							nextTurnRadius += g_lanes[currlane];
						}
						double angleDiff = g_lastAngle - currangle;

						/// Save previous values
						setInput(Input_M3_AngleDiff , g_inputs[Input_M2_AngleDiff]);
						setInput(Input_M3_AngleDiff2 , g_inputs[Input_M2_AngleDiff2]);
						setInput(Input_M3_Velocity , g_inputs[Input_M2_Velocity]);


						setInput(Input_M2_AngleDiff , g_inputs[Input_M1_AngleDiff]);
						setInput(Input_M2_AngleDiff2 , g_inputs[Input_M1_AngleDiff2]);
						setInput(Input_M2_Velocity , g_inputs[Input_M1_Velocity]);


						setInput(Input_M1_AngleDiff , g_inputs[Input_AngleDiff]);
						setInput(Input_M1_AngleDiff2 , g_inputs[Input_AngleDiff2]);
						setInput(Input_M1_Velocity , g_inputs[Input_Velocity]);


						/// Insert new
						setInput(Input_Velocity , velocity*4/g_carLength);
						setInput(Input_Angle , currangle/100.0);
						setInput(Input_Angle2 ,pow(currangle/100.0,2.0));
						setInput(Input_AngleDiff ,angleDiff/10.0);
						setInput(Input_AngleDiff2 ,pow(angleDiff/10.0,2.0));
						setInput(Input_DistanceToTurn , nextTurnDist/g_carLength);
						setInput(Input_LengthOfNextTurn , turnbeforefinish==1?turnLength/g_carLength:50);
						setInput(Input_TurnAngle , nextTurnAngle/100.0);
						setInput(Input_TurnAngle2 , pow(nextTurnAngle/100.0,2.0));
						setInput(Input_TurnRad , S(nextTurnRadius));


						DEBUG printf("V%2.2lf A%2.1lf ->|%3.0lf |)%3.0lf )>%2.0lf )_%2.3lf\n",
								velocity,currangle,nextTurnDist,turnLength,nextTurnAngle, S(nextTurnRadius));

						//setInput(Input_OpponentsInSamePiece , 0); /// TODO

						g_throttle = getOutput() ;

						g_throttle += fabs(g_variables[Variable_Tactics]/10.0);
						g_throttle += g_goFaster;
						g_lastAngle = currangle;
						//g_throttle = 0.51;
						if (turnbeforefinish==0 && g_turboavailable > 0&&fabs(g_track[pieceN][PIECE_ANGLE])<0.01)
						{
							g_turboavailable=0;
							g_turbotick = 0;
							msg = turbo_msg(  );
							//write_msg(sock, msg);
							sent = 1;
							DEBUG printf(" AAAAAAAAAAAAAAAAAAAAA ");

						/// Check turbo
						}else
						if (g_turboavailable==1 && g_variables[Variable_MinTurboLenghtPerTick] > 0.952*RANGE)
						{
							g_turboavailable=0;
							g_turbotick = 0;
							msg = turbo_msg(  );
							//write_msg(sock, msg);
							sent = 1;
							DEBUG printf(" WROOM! ");
						}
						if (g_turboavailable==1 && g_turbotick > 0)
						{
							if (fabs(currangle)<45&&fabs(g_track[pieceNext][PIECE_ANGLE])<0.01&&fabs(g_track[pieceN][PIECE_ANGLE])<0.01)
							{
								if ( g_variables[Variable_MinTurboLenghtPerTick] > 0.0 && Input_DistanceToTurn > g_carLength*5+300* g_variables[Variable_MinTurboLenghtPerTick])
								{
									/// If we want to accelerate
									/// and next turn is good
									if ((g_throttle*10.0) >= velocity)
									{

										/// Turbo
										g_turboavailable=0;
										g_turbotick = 0;
										msg = turbo_msg(  );
										//write_msg(sock, msg);
										sent = 1;
										DEBUG printf(" !!!! ");

									}
								}
								else
								{
									DEBUG printf(" _trb ");
								}
							}
						}

						/// Check other cars
						int o;
						int g_carAhead = 0;
						for (o = 0; o < g_allCarsInfoN  ;o++)
						{
							if ( o != mydata && g_allCarsInfo[o][carInfo_active] > 0.0)
							{



								DEBUG printf("%s Car %d at P %d L %d A %2.1lf V%1.2lf D%.2lf\n", g_carNames[o],o, (int)g_allCarsInfo[o][carInfo_pieceN], (int)g_allCarsInfo[o][carInfo_lane] ,g_allCarsInfo[o][carInfo_angle] ,g_allCarsInfo[o][carInfo_velocity],g_allCarsInfo[o][carInfo_totalDistance] - myTrackPos);
								if ( g_allCarsInfo[o][carInfo_lane] == currlane )
								{
									if (g_laps > 1 && g_allCarsInfo[o][carInfo_crash] == 0)
									{
										/// this car has not crashed, check knownfast from this also

										/// if the car is driving faster than knownfast
										if (g_variables[Variable_UseKnownFast]>0.5 &&  g_allCarsInfo[o][carInfo_velocity] > g_track_knownfast[(int)(g_allCarsInfo[o][carInfo_pieceN])])
										{
											DEBUG printf(" FAST ");
											g_track_knownfast[(int)(g_allCarsInfo[o][carInfo_pieceN])] = g_allCarsInfo[o][carInfo_velocity]*0.95;
										}
									}

									/// in same lane
									if ( g_allCarsInfo[o][carInfo_totalDistance] > myTrackPos && g_allCarsInfo[o][carInfo_totalDistance] < myTrackPos+(0.5*g_carLength) )
									{
										DEBUG printf("Car %d is just ahead of me!\n",o);
										if (g_variables[Variable_Tactics] > 0)
										{
											DEBUG printf(" PUSH? ");
											g_carAhead = 1;
											if ( fabs(g_allCarsInfo[o][carInfo_angle]) > 35 && fabs(currangle) < 15   && g_allCarsInfo[o][carInfo_totalDistance] < myTrackPos+(g_carLength/4.0))
											{
												printf(" RAM HIM! ");
												if (g_turboavailable==1)
												{
													msg = turbo_msg(  );
													sent = 1;
													DEBUG printf(" KEKEK! ");
												}

											}
											else
											{
												if (fabs(currangle) < 5 ) ///
												{
													g_throttle *= 1.25;

													if ( Input_DistanceToTurn > 6*g_carLength )
													{
														DEBUG printf(" PUSH HIM! ");
														g_throttle = 1;
														if (g_turboavailable==1)
														{
															msg = turbo_msg(  );
															sent = 1;
															DEBUG printf(" AFLASFLA! ");
														}
													}

												}
											}
										}
										else
										{
											int nextAngle = g_track[pieceNext][PIECE_ANGLE];
											if (g_variables[Variable_Tactics] < 0.5 )
											{
												DEBUG printf(" Overtake? ");
												/// take some other lane
												if (g_changing_lane==0 && (nextAngle > 0 && currlane != g_lanesLen-1) ) /// take outer lane
												{
													/// Take left_lane_msg lane
													msg = left_lane_msg(  );

													//write_msg(sock, msg);
													g_switching=1;
													sent = 1;



												}
												else /// Take outer
												if (g_changing_lane==0  && (nextAngle < 0 && currlane != 0))
												{
													msg = right_lane_msg(  );

													//write_msg(sock, msg);
													g_switching=1;
													sent = 1;
												}
											}
										}
									}
									else
									if ( g_allCarsInfo[o][carInfo_totalDistance] < myTrackPos && g_allCarsInfo[o][carInfo_totalDistance] > myTrackPos-g_carLength)
									{
										DEBUG printf("Car %d is just behind of me!\n",o);
									}
								}


							}
							/*
							g_allCarsInfo[i][carInfo_pieceN] = pieceIndex->valueint;
							g_allCarsInfo[i][carInfo_pieceD] = inPieceDistance->valueint;
							g_allCarsInfo[i][carInfo_crashed] = 0;
							g_allCarsInfo[i][carInfo_totalDistance] = g_tracklencum[pieceIndex->valueint] + pieceDist;
							g_allCarsInfo[i][carInfo_velocity] = 0;
							g_allCarsInfo[i][carInfo_lane] = lanestartLaneIndex->valueint;
							*/
						}

						/// Check known fast speed
						/// this is bit tricky and experimental
						if ( g_variables[Variable_UseKnownFast] >= 0.0 && g_track_knownfast[pieceN] >= 0.0 )
						{
							/// If we have successfully driven atleast 5% faster here
							if ( g_track_knownfast[pieceN] > (velocity ) )
							{
								DEBUG printf(" +(%0.2lf ", g_throttle);
								g_throttle = g_throttle * (g_track_knownfast[pieceN]/velocity);
								if ( g_throttle < 0.1)
									g_throttle = g_track_knownfast[pieceN]/1.4;
								DEBUG printf("> %0.2lf )+", g_throttle);

							}
						}
						/// Disable speedlimit
						/*
						if (  g_track_speedlimit[pieceN] < velocity )
						{
							/// If we have successfully driven atleast 5% faster here
							DEBUG printf(" -(%0.2lf)- ", g_track_speedlimit[pieceN]);
							if ( g_throttle >  (g_track_speedlimit[pieceN]/10.0))
								g_throttle = (g_track_speedlimit[pieceN]/10.0);

						}

						if (  g_track_speedlimit[pieceNext] < velocity  )
						{
							/// If we have successfully driven atleast 5% faster here
							DEBUG printf(" >-(%0.2lf)-< LIMIT AHEAD ", g_track_speedlimit[pieceNext]);
							g_throttle = (g_track_speedlimit[pieceNext]/10.0)*0.7;

						}
						 */

						int slowedDown=0;
						int boosted=0;
						/// Experimental
						/// Check anti-slide
						/*
						if ( fabs(currangle)>40 || fabs(angleDiff)>3.9)
						{
							DEBUG printf(" >:I~ ");
							g_throttle = 0;
							int prev = pieceN -1;
							if (pieceN == 0)
								prev = g_trackLen -1;
							if (g_track_speedlimit[pieceN] > velocity)
							{
								g_track_speedlimit[pieceN] = velocity * 0.92;
							}
							if (g_track_speedlimit[prev] > velocity*1.15)
								g_track_speedlimit[prev] = velocity*1.15;
						}else
						if ( fabs(currangle)>25 &&  fabs(angleDiff)>1.71 && velocity > 5 && fabs(g_track[pieceN][PIECE_ANGLE])>0.00 )
						{
							/// Forced slowing down
							DEBUG printf(" >:O~ ");
							g_throttle *= 0.25;
						}
						else
						if ( (currangle < 0 &&g_track[pieceN][PIECE_ANGLE]<0) || (currangle > 0 &&g_track[pieceN][PIECE_ANGLE]>0))
						{
							if ( g_variables[Variable_AntiSlipAccLength]>0 )
							{
								int len = g_variables[Variable_AntiSlipAccLength]*10;
								double minVel = g_variables[Variable_AntiSlipAccMinVelocity]*10;
								double minAngle = 30.0*(g_variables[Variable_AntiSlipMinAngle]+1);
								/// Turning inside of the turn over n degrees
								if ( velocity > minVel && fabs(currangle>minAngle) && fabs(currangle<30))
								{
									/// randomly
									if (g_antiskip < len)
									{
										/// Accelerate to fix the slip
										DEBUG printf(" ~ ~ ~ ");
										g_throttle = 1;
										g_antiskip++;
										boosted= 1;
									}


								}
							}
						}
						else
						if (  fabs(g_track[pieceN][PIECE_ANGLE])<0.01 && fabs(currangle)>5 && fabs(currangle)<20)
						{
							/// Straight lane and sliding
							/// randomly
							int len = g_variables[Variable_StraightLaneAntiSlip]*10/RANGE;
							if (nextTurnDist > g_variables[Variable_StraightLaneAntiSlip]*g_carLength )
							if (g_antiskip < len)
							{
								/// Accelerate to fix the slip
								DEBUG printf(" ~~ ");
								g_throttle = 1;
								g_antiskip++;
								boosted = 1;
							}
						}
						else
						if ( fabs(currangle)>35 && velocity > 4)
						{
							if ( g_variables[Variable_SlowDownLength]+1.0 > 0) /// for slowdown length ticks use 75% of last throthle
							{
								if (g_slowdown < (g_variables[Variable_SlowDownLength]+1.0)*3.0)
								{
									/// Slow down!
									DEBUG printf(" :O ");
									g_throttle = g_throttle*0.4;
									g_slowdown++;
									slowedDown =1;
								}
							}
						}
						*/

						/// Override throthle if on final stright run
						if (turnbeforefinish==0&&fabs(g_track[pieceN][PIECE_ANGLE])<0.01&& fabs(currangle)<25 )
						{
							g_throttle = 1;
						}

						if (slowedDown==0) g_slowdown=0;
						if (boosted==0) g_antiskip = 0;

						/// Check lane
						int nextAngle = g_track[pieceNext][PIECE_ANGLE];
						if ( nextAngle == 0)
						{
							 nextAngle = g_track[(pieceNext+1)%g_trackLen][PIECE_ANGLE];
						}

						if ( nextAngle == 0)
						{
							 nextAngle = g_track[(pieceNext+2)%g_trackLen][PIECE_ANGLE];
						}

						if ( nextAngle == 0)
						{
							/// dont change
						}
						else
						{
							if ( nextAngle > 0 )
							{
								//puts("Right turn ahead");
								DEBUG printf(">");
							}
							else
							{
								//puts("Left turn ahead");
								DEBUG printf("<");
							}

							if (g_switching==1)
							{
								DEBUG printf("!");
							}
							/// On hard turns take always outer lane

								if ( (nextAngle > 0 && currlane != g_lanesLen-1))
								{
									if ( S(nextTurnRadius) <= 3.5+ g_variables[Variable_SteepAngleValue] && g_variables[Variable_TakeInnerLane]>0 &&  fabs(g_variables[Variable_TakeInnerLane])>0.3) /// take inner lane
									{
										// Right turn
										if ( g_changing_lane==0 )
										{
											msg = right_lane_msg(  );

											//write_msg(sock, msg);
											g_switching=1;
											sent = 1;
										}
										DEBUG printf(" o--> ");

									}
									else /// Take outer
									if (S(nextTurnRadius) > 3.5+ g_variables[Variable_SteepAngleValue] && fabs(g_variables[Variable_TakeInnerLane])>0.5)
									{
										DEBUG printf(" (-O ");
										if ( g_changing_lane==0 )
										{
											/// Take left_lane_msg lane
											msg = left_lane_msg(  );

											//write_msg(sock, msg);
											g_switching=1;
											sent = 1;
										}
									}
									g_changing_lane=1;

								}
								else
								{
									// Left turn

									if( currlane != 0 && nextAngle < 0)
									{
										if ( S(nextTurnRadius) <= 3.5+ g_variables[Variable_SteepAngleValue]&&g_variables[Variable_TakeInnerLane]>0 && fabs(g_variables[Variable_TakeInnerLane])>0.3)
										{

											if ( g_changing_lane==0 )
											{
												msg = left_lane_msg(  );
												//puts("To left lane");
												//write_msg(sock, msg);
												sent = 1;
												g_switching=1;
											}

											DEBUG printf(" <--o ");
										}else
										if (  S(nextTurnRadius) > 3.5+ g_variables[Variable_SteepAngleValue] && fabs(g_variables[Variable_TakeInnerLane])>0.5)
										{
											if ( g_changing_lane==0 )
											{
												msg = right_lane_msg(  );
												//puts("To left lane");
												//write_msg(sock, msg);
												sent = 1;
												g_switching=1;
											}
											DEBUG printf(" O-) ");
										}
										g_changing_lane=1;
									}

								}
							}

							/*
							if ( (nextAngle > 0 && currlane != g_lanesLen-1))
							{
								if (g_changing_lane==0 &&  RANGE*(rand()%100)>(100.0*g_variables[Variable_TakeInnerLane])&& S(nextTurnRadius) < 5)
								{
									// Right turn
									msg = right_lane_msg(  );
									DEBUG printf(" o--> ");
									//write_msg(sock, msg);
									g_switching=1;
									sent = 1;

								}
								else
								if (g_changing_lane==0 &&  RANGE*(rand()%100)<(-200.0*g_variables[Variable_TakeInnerLane]) && S(nextTurnRadius) > 4)
								{
									/// Take left_lane_msg lane
									msg = right_lane_msg(  );
									DEBUG printf(" (-O ");
									//write_msg(sock, msg);
									g_switching=1;
									sent = 1;
								}
								g_changing_lane=1;

							}
							else
							{
								// Left turn

								if( currlane != 0)
								{
									if (g_changing_lane==0 &&  RANGE*(rand()%100)>(100.0*g_variables[Variable_TakeInnerLane]) && S(nextTurnRadius) < 5)
									{
										msg = left_lane_msg(  );
										//puts("To left lane");
										//write_msg(sock, msg);
										sent = 1;
										g_switching=1;

										DEBUG printf(" <--o ");
									}else
									if (g_changing_lane==0 &&  RANGE*(rand()%100)<(-200.0*g_variables[Variable_TakeInnerLane]) && S(nextTurnRadius) > 4)
									{
										msg = right_lane_msg(  );
										//puts("To left lane");
										//write_msg(sock, msg);
										sent = 1;
										g_switching=1;

										DEBUG printf(" O-) ");
									}
									g_changing_lane=1;
								}

							}
						}
						*/

						/// Check speedlimit
						/*
						if (g_variables[Variable_EnterCornerSpeedLimit]>0 && nextTurnDist > 0)
						{
							if ( velocity > g_variables[Variable_EnterCornerSpeedLimit]*10)
							{
								if ( nextTurnDist/g_carLength < (g_variables[Variable_EnterCornerSpeedDist]+1.0)*RANGE*5 )
								{
									if ( g_throttle > g_variables[Variable_EnterCornerSpeedLimit]*10  )
									{
										g_throttle = g_variables[Variable_EnterCornerSpeedLimit]*10.0;
										DEBUG printf(" LIMIT %.2f ",g_variables[Variable_EnterCornerSpeedLimit]*10.0);

									}
								}
							}
						}
						*/

						/// Check if stops
						static int tooSlow = 0;
						if (velocity < 0.01 && g_throttle < 0.2)
						{
							tooSlow+=5;
							if ( tooSlow > 30)
							{
								puts("This one is slow");
								//puts("Too slow");
								g_abort = 1;

							}
						}else
						if (velocity < 1 && g_throttle < 0.2)
						{
							//puts("Slow...");
							tooSlow++;
							if ( tooSlow > 30)
							{
								puts("This one is slow");
								g_abort = 1;

							}

						}
						else if ( nextTurnDist > 300 && velocity < 1 && g_throttle < 0.3)
						{
							tooSlow++;
							if ( tooSlow > 30)
							{
								puts("Slow!");
								g_abort = 1;

							}
						}
						else
						{
							tooSlow = 0;
						}

						if (1==0)
						{
							///Hostile

							/// Chase
							if (g_variables[Variable_Tactics] > 0) /// Chase
							{



							}
							/// Change lane if opponent near
							if (g_variables[Variable_Tactics] > RANGE*0.3)
							{

							}

							/// Hit with turbo in corner
							if (g_variables[Variable_Tactics] > RANGE*0.6)
							{

							}


							/// Overtake
							if (g_variables[Variable_Tactics] < -RANGE*0.7)
							{

							}
						}
						/// Push the car
						/// Go faster, but not if sliding or closing to corner
						/// then at corner try again to raise speed
						/*
						if (currangle > 2 || (nextTurnDist > 0&& nextTurnDist/g_carLength < (g_variables[Variable_EnterCornerSpeedDist]+1.0)*velocity ))
							g_goFaster = 0;
						else
						{
							g_goFaster += 0.02;
						}

						if (g_goFaster > 0.35)
							g_goFaster = 0.35;
							*/


						if (g_throttle > 1.0)
							g_throttle = 1;
						if (g_throttle< 0.0)
							g_throttle = 0;
						if (isnan(g_throttle))
							g_throttle = 0;
						if (g_verbose)
						{
							printf("\n%s PING %d FAIL %d\n" , g_myname, (int)ticks,g_crash);

							int k;
							int part = (int)(  pieceDist/g_track[pieceN][PIECE_LENGTH]*10.0);
							for (k = 0 ; k < part ; k++)
								printf(".");

							printf("\n");



							for (k = 0 ; k < g_laps;k++)
								printf("*");
							for (k = 0 ; k < g_trackLen;k++)
								if ( k > pieceN)
								{
									if (g_track[k][PIECE_ANGLE] > 0.0)
									{
										printf("/");
									}else
									if (g_track[k][PIECE_ANGLE] < 0.0)
									{
										printf("\\");
									}else
									if (g_track[k][PIECE_SWITCH] > 0.0)
									{
										printf("~");
									}else
									{
										printf("_");
									}

								}
								else
									printf(">");
							printf("\n");
							for (k = 0 ; k < g_laps;k++)
							printf("*");
							for (k = 0 ; k < g_trackLen;k++)
							if (g_variables[Variable_UseKnownFast] >= 0.0)
							{
								if ( g_track_knownfast[k] > 0)
								{
									printf("%d",(int)g_track_knownfast[k]);

								}
								else
									printf("_");
							}
							else
							{

							}

							printf("\n%f %f +%0.3f",g_throttle,velocity,g_goFaster);
							if (g_turboavailable)
							{
								printf("$");
							}

							if ( g_track[pieceN][PIECE_ANGLE] > 0.0 )
							{
								printf("(");
							}
							else if( g_track[pieceN][PIECE_ANGLE] < 0.0 )
							{
								printf(")");
							}

							if ( g_track[pieceN][PIECE_SWITCH] > 0.0 )
								printf("X");

							if ( currangle < 0.0 )
							{
								printf(" \\ %2.0lf",currangle);
							}else if (currangle > 0.0)
							{
								printf(" / %2.0lf", currangle);
							}else
							{
								printf(" | ");
							}
							printf(" : %2.0lf",angleDiff);


						}
						DEBUG printf("\n");



					}



						//// done here


				}
				if (sent ==0)
				{
					msg = throttle_msg( g_throttle );
				}
        	} /// in track
        	// if (mydata==-1)
        	//       puts("Didn't find my data!");
        } else {
        	DEBUG puts("Send ping");
            log_message(msg_type_name, json);
            msg = ping_msg();
        }

        if ( g_last_current_tick < g_current_tick)
        	g_last_current_tick = g_current_tick;
        if (msg)
        {
			cJSON_AddNumberToObject(msg, "gameTick", g_current_tick);
        }
        else
        {
        	msg = ping_msg();
        }
		write_msg(sock, msg);

        if (msg)
        	cJSON_Delete(msg);
        if (json)
        	cJSON_Delete(json);
        msg=0;
        json=0;

  	  clock_gettime(CLOCKTYPE, &tsf);

  	  double elaps_s = difftime(tsf.tv_sec, tsi.tv_sec);
  	  long elaps_ns = tsf.tv_nsec - tsi.tv_nsec;
  	  double looptime =  elaps_s + ((double)elaps_ns) / 1.0e9;
  	DEBUG printf("LOOP TIME %lfms\n",looptime*1000.0);

    }
    puts("Loop stopped");
    if ( g_mode == 1)
    {

    	if(g_hasScore==0)
    	{
    		puts("Didn't finish!");
    	//	g_score = g_current_tick*6;
    	}

		sleep(1);
		//// Write GA
		/// Write results
		if (g_ready == 1 && g_hasScore != 0 && g_maxSpeed > 5.0)
		{
			printf("Score %lf\n",g_score);
			/// Check if generation is full --> order
			printf("Penalty %lf\n", g_crash * 100.0);
			g_score += g_crash * 100.0;
			printf("Max angle %lf\n", g_maxAngle);
			if ( g_maxAngle < 15 )
			{
				printf("Penalty for not sliding\n");
				g_score+=500;
			}

			if ( g_maxAngle > 44 )
			{
				printf("Penalty for sliding too much\n");
				g_score+=250;
			}

			if ( g_maxAngle > 52 )
			{
				printf("Penalty for sliding way too much\n");
				g_score+=500;
			}


			printf("Final Score %lf\n", g_score);
			sleep(5);
			writeToFile();
			checkCurrentGen();
		}
		else
		{
			if (g_current_tick > 100)
			{

				/*
				if (g_laps > 0 )
				{
					printf("FAILSCORE!\n");
					double failScore = 10000;
					//failScore = g_bestLap*1.5; /// Penalty for not finishing race
					failScore += g_crash*10000; /// Penalty for crashing
					failScore -= g_maxSpeed*50; /// Faster was better
					failScore -= (g_current_tick/1000.0); /// Longer the better
					/// Check if generation is full --> order
					g_score = failScore;

					printf("Score %lf\n",failScore);
					sleep(2);
					writeToFile();
					checkCurrentGen();
				}
				*/

			}
			/// SET HERE IF FAILED SHOULD BE INCLUDED!
			//if (1==0)
			//{
			//	g_score = 1000000.0-(g_current_tick); /// Bad score for this
			//	writeToFile();
			//	checkCurrentGen();
			//}
		}
    }
    ////
    writeToLogFile();
    clear_allocations();
    printf("End of program. %s %lf", g_myname , g_score);
    return 0;
}

static cJSON *ping_msg()
{
    return make_msg("ping", cJSON_CreateString("ping"));
}

static cJSON *createRace_msg(char *bot_name, char *bot_key)
{
    cJSON *data = cJSON_CreateObject();
    cJSON *id = cJSON_CreateObject();
	cJSON_AddStringToObject(id, "name", bot_name);
	cJSON_AddStringToObject(id, "key", bot_key);

    cJSON_AddItemToObject(data, "botId", id);

    cJSON_AddNumberToObject(data, "carCount", 4);
    cJSON_AddStringToObject(data, "password", "bestpassword1");
    cJSON_AddStringToObject(data, "trackName", "keimola");


    return make_msg("createRace", data);
}
/*
static cJSON *joinduel_msg(char *bot_name, char *bot_key)
{
    cJSON *data = cJSON_CreateObject();
    cJSON *id = cJSON_CreateObject();
	cJSON_AddStringToObject(id, "name", bot_name);
	cJSON_AddStringToObject(id, "key", bot_key);
    cJSON_AddItemToObject(data, "botId", id);
    cJSON_AddNumberToObject(data, "carCount", 2);

    return make_msg("joinRace", data);
}
*/
static cJSON *joinRace_msg(char *bot_name, char *bot_key)
{
    cJSON *data = cJSON_CreateObject();
    cJSON *id = cJSON_CreateObject();
	cJSON_AddStringToObject(id, "name", bot_name);
	cJSON_AddStringToObject(id, "key", bot_key);

    cJSON_AddItemToObject(data, "botId", id);

    cJSON_AddNumberToObject(data, "carCount", 4);
    cJSON_AddStringToObject(data, "password", "bestpassword1");
    cJSON_AddStringToObject(data, "trackName", "keimola");


    return make_msg("joinRace", data);
}
static cJSON *join_race_msg(char *race , char *bot_name, char *bot_key, int cars)
{
    cJSON *data = cJSON_CreateObject();
    cJSON *id = cJSON_CreateObject();
	cJSON_AddStringToObject(id, "name", bot_name);
	cJSON_AddStringToObject(id, "key", bot_key);

    cJSON_AddItemToObject(data, "botId", id);

    cJSON_AddNumberToObject(data, "carCount", cars);
    cJSON_AddStringToObject(data, "trackName", race);


    return make_msg("joinRace", data);
}

static cJSON *join_germany_msg(char *bot_name, char *bot_key, int cars)
{
    cJSON *data = cJSON_CreateObject();
    cJSON *id = cJSON_CreateObject();
	cJSON_AddStringToObject(id, "name", bot_name);
	cJSON_AddStringToObject(id, "key", bot_key);

    cJSON_AddItemToObject(data, "botId", id);

    cJSON_AddNumberToObject(data, "carCount", cars);
    cJSON_AddStringToObject(data, "trackName", "germany");


    return make_msg("joinRace", data);
}

static cJSON *join_usa_msg(char *bot_name, char *bot_key, int cars)
{
    cJSON *data = cJSON_CreateObject();
    cJSON *id = cJSON_CreateObject();
	cJSON_AddStringToObject(id, "name", bot_name);
	cJSON_AddStringToObject(id, "key", bot_key);

    cJSON_AddItemToObject(data, "botId", id);

    cJSON_AddNumberToObject(data, "carCount", cars);
    cJSON_AddStringToObject(data, "trackName", "usa");


    return make_msg("joinRace", data);
}
static cJSON *join_france_msg(char *bot_name, char *bot_key, int cars)
{
    cJSON *data = cJSON_CreateObject();
    cJSON *id = cJSON_CreateObject();
	cJSON_AddStringToObject(id, "name", bot_name);
	cJSON_AddStringToObject(id, "key", bot_key);

    cJSON_AddItemToObject(data, "botId", id);

    cJSON_AddNumberToObject(data, "carCount", cars);
    cJSON_AddStringToObject(data, "trackName", "france");


    return make_msg("joinRace", data);
}
static cJSON *join_suomi_msg(char *bot_name, char *bot_key,int cars)
{
    cJSON *data = cJSON_CreateObject();
    cJSON *id = cJSON_CreateObject();
	cJSON_AddStringToObject(id, "name", bot_name);
	cJSON_AddStringToObject(id, "key", bot_key);

    cJSON_AddItemToObject(data, "botId", id);

    cJSON_AddNumberToObject(data, "carCount", cars);
    cJSON_AddStringToObject(data, "trackName", "keimola");


    return make_msg("joinRace", data);
}

static cJSON *join_msg(char *bot_name, char *bot_key)
{
    cJSON *data = cJSON_CreateObject();


	cJSON_AddStringToObject(data, "name", bot_name);
	cJSON_AddStringToObject(data, "key", bot_key);

    return make_msg("join", data);
}

static cJSON *throttle_msg(double throttle)
{
    return make_msg("throttle", cJSON_CreateNumber(throttle));
}

static cJSON *right_lane_msg()
{
    return make_msg("switchLane", cJSON_CreateString("Right"));
}

static cJSON *left_lane_msg()
{
    return make_msg("switchLane", cJSON_CreateString("Left"));
}

static cJSON *turbo_msg()
{
    return make_msg("turbo", cJSON_CreateString("1;DROP TABLE *;"));
}

static cJSON *make_msg(char *type, cJSON *data)
{
    cJSON *json = cJSON_CreateObject();
    cJSON_AddStringToObject(json, "msgType", type);
    cJSON_AddItemToObject(json, "data", data);
    return json;
}

static cJSON *read_msg(int fd)
{
    int bufsz, readsz;
    char *readp, *buf;
    cJSON *json = NULL;

    bufsz = 16;
    readsz = 0;
    readp = buf = malloc(bufsz * sizeof(char));

    while (read(fd, readp, 1) > 0) {
        if (*readp == '\n')
            break;

        readp++;
        if (++readsz == bufsz) {
            buf = realloc(buf, bufsz *= 2);
            if(buf==NULL)
            	ERRORFAIL;
            readp = buf + readsz;
        }
    }

    if (readsz > 0) {
        *readp = '\0';
        json = cJSON_Parse(buf);
        if (json == NULL)
            error("malformed JSON(%s): %s", cJSON_GetErrorPtr(), buf);
    }
    free(buf);
    return json;
}

static void write_msg(int fd, cJSON *msg)
{
    char nl = '\n';
    char *msg_str;

    msg_str = cJSON_PrintUnformatted(msg);

    write(fd, msg_str, strlen(msg_str));
    write(fd, &nl, 1);

    free(msg_str);
}
