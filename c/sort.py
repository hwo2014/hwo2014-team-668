import sys
import csv
import numpy as np
import sys
from operator import itemgetter, attrgetter
if len(sys.argv) < 2:
    print "no filename"
    exit
else:
    
    reader = csv.reader(open(sys.argv[1]), delimiter=" ")
    #for row in reader:
    #    print row[0]
    sortedlist = sorted(reader,key=lambda x: float(x[0]))

    with open(sys.argv[1]+'.sort', 'wb') as csvfile:
        out = csv.writer(csvfile, delimiter=' ',
                                quotechar='|', quoting=csv.QUOTE_MINIMAL)
        for i in sortedlist:
            out.writerow(i)
