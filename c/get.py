import sys
import csv
import numpy as np
import sys
from operator import itemgetter, attrgetter

    
reader = csv.reader(open(sys.argv[1]), delimiter=" ")
reader2 = csv.reader(open(sys.argv[2]), delimiter=" ")
#for row in reader:
#    print row[0]
sortedlist1 = sorted(reader,key=lambda x: float(x[0]))
sortedlist2 = sorted(reader2,key=lambda x: float(x[0]))
a = []
b = []
for i in sortedlist1:
	a.append(i)

for i in sortedlist2:
	b.append(i)

#print a
#print b
res = []

c = 1
i0 = 0
i1= 0
for g1 in a:
	#res.append([])
	i1 = 0
	for g2 in b:
#		print "i:",i0,
#		print "j:",i1
		r = [i0,i1]
		max_diff = 0
		total_diff = 0
		for i in range(1,len(g2)):
			r.append(float(g1[i])-float(g2[i]))
			total_diff = total_diff + abs(float(g1[i])-float(g2[i]))
			if abs(float(g1[i])-float(g2[i])) > max_diff:
				max_diff = abs(float(g1[i])-float(g2[i]))
		r.append(total_diff)
		r.append(max_diff)
		res.append(r)
		
		i1 = i1 + 1
	i0 = i0 + 1	
	c = c + 1
#for x in res:
#	print x
	
with open("results.csv", 'wb') as csvfile:
	out = csv.writer(csvfile, delimiter='|',
							quotechar='#', quoting=csv.QUOTE_MINIMAL)
	for x in res:
		out.writerow(x)
		

